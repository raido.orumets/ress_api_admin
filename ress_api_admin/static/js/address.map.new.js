/*---------------------------------------------------
 * Address: Map (NEW)
 *---------------------------------------------------
 * X - Latitude - N
 * Y - Longitude - E
 *--------------------------------------------------*/

function getDefaultMapCoordinates() {

    return { // Estonia coordinates
        latitude: 58.5953,
        longitude: 25.0136,
        zoom: 7.4,
    };
}

function showOpenLayersMap(targetElement, latitude, longitude, zoom) {

    if (!latitude || !longitude) { // Default coordinates
        let defaultCoords = getDefaultMapCoordinates();

        latitude = defaultCoords.latitude;
        longitude = defaultCoords.longitude;
        zoom = defaultCoords.zoom;
    }

    let point = ol.proj.fromLonLat([longitude, latitude]);

    let iconFeature = new ol.Feature({
        geometry: new ol.geom.Point(point),
        name: 'Sinu asukoht',
    });

    let view = new ol.View({ // Show map from coordinates
        center: point,
        zoom: (zoom) ? zoom : 17, // Default aadress zoom: 17
    });

    if ($('#' + targetElement).empty()) { // Prevents loading multiple maps into "target" <div>

        // Init map
        let map = new ol.Map({
            target: targetElement,
            onFocusOnly: false,
            controls: ol.control.defaults().extend([
                new ol.control.FullScreen()
                /* var fullscreenTarget = document.getElementById('info').parentElement;
                new ol.control.FullScreen({
                    source: fullscreenTarget
                }); */
            ]),
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM(),
                }),
                new ol.layer.Vector({
                    source: new ol.source.Vector({
                        features: [iconFeature],
                    }),
                    style: new ol.style.Style({
                        image: new ol.style.Icon({
                            src: '/static/images/icons/blue/pin.svg',
                            anchorXUnits: 'fraction',
                            anchorYUnits: 'pixels',
                            anchor: [0.5, (64 - 4)], // [iconCenter, ActualIconHeight] (64-5) => (ActualIconHeight - iconMarginBottom)
                            size: [64, 64], // [ActualIconWidth, ActualIconHeight]
                            scale: 0.8,
                        }),
                    }),
                }),
            ],
            view: view,
        });

        setTimeout(function () { // Assures that map is loaded correctly after tab click
            map.updateSize();
        }, 300);

        // Popover showing the position the user clicked
        let popup = new ol.Overlay({
            element: document.getElementById('popup'),
        });
        map.addOverlay(popup);
        map.on('click', function (evt) {
            let element = popup.getElement();
            let coordinate = evt.coordinate;
            let hdms = toStringHDMS(toLonLat(coordinate));

            $(element).popover('dispose');
            popup.setPosition(coordinate);
            $(element).popover({
                container: element,
                placement: 'top',
                animation: false,
                html: true,
                content: '<p>The location you clicked was:</p><code>' + hdms + '</code>',
            });
            $(element).popover('show');
        });

        // Show x and y coordinates at the top right corner
        let mousePosition = new ol.control.MousePosition({
            coordinateFormat: ol.coordinate.createStringXY(7),
            projection: 'EPSG:4326',
            target: document.getElementById('myposition'),
            undefinedHTML: '&nbsp;'
        });
        map.addControl(mousePosition);

        // Change mouse grap cursor
        map.getViewport().style.cursor = "-webkit-grab";
        map.on('pointerdrag', function (evt) {
            map.getViewport().style.cursor = "-webkit-grabbing";
        });
        map.on('pointerup', function (evt) {
            map.getViewport().style.cursor = "-webkit-grab";
        });

        // TODO: Make tooltips appeat only large maps
        // Tooltips
        /* $('.ol-zoom-in, .ol-zoom-out').tooltip({
            //container: '#' + targetElement,
            boundary: 'window',
            placement: 'right',
        });
        $('.ol-full-screen.ol-control button').tooltip({
            //container: '#' + targetElement,
            boundary: 'window',
            placement: 'left',
        }); */
    }

    // TODO: https://openlayers.org/en/latest/examples/geographic.html
}

function showMapFullscreenModal() {
    let modal = $('#modal-map-search-fullscreen');
    modal.modal('show');

    // TODO: Fullscreen search mode
}

function getLatLonFromXY(coord_x, coord_y) {
    // TODO: Why X and Y needs to be mixed in lest2geo() ?
    let coords = lest2geo(coord_y / 100, coord_x / 100)

    return {
        latitude: coords.lat,
        longitude: coords.lon,
    };
}

function getCoordinatesFromUrl() {
    let latitude = null;
    let longitude = null;

    if (getUrlParam('building_lat') && getUrlParam('building_lon')) {

        // TODO: Why LAT and LON are changed in Addressregistry response ?
        latitude = getUrlParam('building_lon');
        longitude = getUrlParam('building_lat');

        console.info('GET URL params building_lat: ' + latitude + ' and building_lon: ' + longitude);

    } else if (getUrlParam('coord_x') && getUrlParam('coord_y')) {

        let coords = getLatLonFromXY(getUrlParam('coord_x'), getUrlParam('coord_y'));

        latitude = coords.latitude;
        longitude = coords.longitude;

        console.info('GET URL params coord_x: ' + latitude + ' and coord_y: ' + longitude);
    } else {
        console.warn('GET URL params: Params are not defined.');
    }

    return {
        latitude: latitude,
        longitude: longitude,
    };
}

function showUrlLocationOnMap() {

    let targetElement = 'ol-map-content';
    let zoom = 17;
    let coords = getCoordinatesFromUrl();

    if (!coords.latitude || !coords.longitude) {
        showNotificationModal('icon-pin-blue', 'Ei saa kuvada', 'Ei saa kuvada asukohta kaardil, kuna URL parameetrid puuduvad.', 'Sulge teade', 'coord_x, coord_y või building_lat, building_lon');
    }

    showOpenLayersMap(targetElement, coords.latitude, coords.longitude, zoom);
}

function showUrlLocationOnGoogleMaps(showInModal) {

    let coords = getCoordinatesFromUrl();

    if (coords.latitude && coords.longitude) {

        let googleMapsUrl = `http://www.google.com/maps/place/${coords.latitude},${coords.longitude}`;

        if (showInModal) {
            showUrlInFullscreenModal(showInModal);
        } else {
            window.open(googleMapsUrl, '_blank');
        }
    } else {
        showNotificationModal('icon-pin-blue', 'Ei saa kuvada', 'Ei saa kuvada asukohta kaardil, kuna URL parameetrid puuduvad.', 'Sulge teade', 'coord_x, coord_y või building_lat, building_lon');
    }
}

function showDefaultLocationOnMap() {

    let targetElement = 'ol-map-content';

    showOpenLayersMap(targetElement, null, null, null);
}

function showLocationOnMap(latitude, longitude, zoom) {

    let targetElement = 'ol-map-content';

    showOpenLayersMap(targetElement, latitude, longitude, zoom);
}

function getCurrentGeolocation(callbackSuccess, callbackError, callbackComplete) {

    if ('geolocation' in navigator) {

        navigator.geolocation.getCurrentPosition(

            function (position) {
                let coord = position.coords;
                let geolocation = {
                    latitude: coord.latitude,
                    longitude: coord.longitude,
                    accuracy: coord.accuracy,
                };

                callbackSuccess(geolocation);
                callbackComplete();

                console.info(`[Geolocation] Your current position: LAT ${coord.latitude}, LON ${coord.longitude}`);
                console.info(`[Geolocation] Your current position more or less ${coord.accuracy} meters.`);
            },

            function (error) {
                let messages = {
                    0: 'Sinu asukoha tuvastamine ebaõnnestus teadmata põhjusel.',
                    1: 'Sa oled otsustanud oma asukohta mitte jagada.<br />OK, me ei küsi seda enam.',
                    2: 'Internet on maas või positsioneerimise funktsionaalsus ei tööta.',
                    3: 'Positsioneerimise päring aegus enne kui sinu positsioon kätte saadi.<br />Palun proovi uuesti.',
                };
                let errorMessage = (messages[error.code]) ? messages[error.code] : messages[0];

                callbackError(error.code, errorMessage);
                callbackComplete();

                console.warn(`[Geolocation] ERROR (${error.code}): ${errorMessage}`);
            },

            {
                enableHighAccuracy: true,
                timeout: 10000,
                maximumAge: 0,
            }
        );

    } else {

        let errorCode = 0;
        let errorMessage = 'Sinu veebibrauser on aegunud ja ei toeta positsioneerimist.';

        callbackError(errorCode, errorMessage);
        callbackComplete();

        console.warn(`[Geolocation] ERROR (${errorCode}): ${errorMessage}`);
    }
}

function showCurrentLocationOnMap() {

    showCardPreloader('#tab-map-search-new-content .card:first');

    getCurrentGeolocation(

        function (geolocation) { // success
            let targetElement = 'ol-map-content';
            let zoom = 17;

            showOpenLayersMap(targetElement, geolocation.latitude, geolocation.longitude, zoom);
        },

        function (errorCode, errorMessage) { // error
            showErrorModal('Viga', errorMessage, errorCode);
            showDefaultLocationOnMap();
        },

        function () { // complete
            hideCardPreloader('#tab-map-search-new-content .card:first');
        }
    );
}

/*---------------------------------------------------
 * Init
 *--------------------------------------------------*/
$(document).on('click', '#nav-map-search-new-tab', function () {

    // Mustamäe tee 3, Tallinn
    let latitude = 59.4284645;
    let longitude = 24.7054263;
    let zoom = 17;

    let targetElement = 'ol-map-content';

    showOpenLayersMap(targetElement, latitude, longitude, zoom);
});
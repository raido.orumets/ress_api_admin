/*---------------------------------------------------
 * Address: Geopank and Google Map
 *--------------------------------------------------*/

function initGeopankIframeMap(targetElement) {

    let origin = window.location.origin;
    let geopankUrl = null;

    if (origin.indexOf("soa-arendus.elion.ee") >= 0) {
        geopankUrl = 'http://geopank-arendus.cc.elion.ee/address/?callback_url=';

    } else if (origin.indexOf("soa-test.elion.ee") >= 0) {
        geopankUrl = 'http://geopank-arendus.cc.elion.ee/address/?callback_url=';

    } else if (origin.indexOf("soa.elion.ee") >= 0) {
        geopankUrl = 'http://geopank.elion.ee/address/?callback_url=';

    } else {
        geopankUrl = 'http://geopank-arendus.cc.elion.ee/address/?callback_url=';
    }

    $(targetElement).html('<iframe class="border-0"' +
        ' src="' + geopankUrl + getGeopankCallbackUrl() +
        '" width="100%"' +
        ' height="600">' +
        ' </iframe>');
}

function initGoogleMapsIframeMap(targetElement) {
    $(targetElement).html('<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1071506.2429961406!2d23.781764071288343!3d58.6067977605823!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4692949c82a04bfd%3A0x40ea9fba4fb425c3!2sEstonia!5e0!3m2!1sen!2see!4v1644434660944!5m2!1sen!2see" height="500" style="width: 100%; border:0;" allowfullscreen="" loading="lazy"></iframe>');
}

/*---------------------------------------------------
 * Init
 *--------------------------------------------------*/
$(document).on('click', '#nav-map-search-tab', function () {

    initGeopankIframeMap('#map-content');
    // initGoogleMapsIframeMap('#map-content');
});
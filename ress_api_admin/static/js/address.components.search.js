function queryLocationsByTextComps() {

    $.ajax({
        url: getGazetteerUrl(),
        type: 'GET',
        data: {
            query_type: 'text_search',
            region: $('#comp_search_region').val(),
            city_county: $('#comp_search_city_county').val(),
            municipality: $('#comp_search_municipality').val(),
            street: $('#comp_search_street_village').val()
        },
        dataType: 'json'

    }).done(function (response) {
        $('#component-search-location-tbody').empty();
        response.forEach(function (locComp) {
            $('#component-search-location-tbody')
                .append([
                    $('<tr>'),
                    $('<td><button id="loc_component_id_' + locComp.id +
                        '" type="button" class="btn btn-primary btn-sm">Vali</button></td>'),
                    $('<td id="loc_comp_name_' + locComp.id + '">' + locComp.name + '</td>'),
                    $('<td id="loc_comp_type_name_' + locComp.id + '">' + locComp.subtype_label + '</td>'),
                    $('</tr>')
                ]);
            $("#loc_component_id_" + locComp.id).click(function () {
                transferComponentTextSearchToHierarchy(this);
            });
        });

    }).always(function () {
        if ($('#component-search-location-tbody').children().length > 1) {
            $('#component-search-location-results').removeClass('d-none');
        } else {
            $('#component-search-location-results').addClass('d-none');
        }
    });
}

function selectLocation(compNameTokens) {
    $('#region').val(compNameTokens[0]);
    $('#city_county').val(compNameTokens[1]);
    $('#municipality').val(compNameTokens[2]);
    $('#street_bg_name').val(compNameTokens[3]);
}

function extractComponentTokens(component) {
    let compIdTokens = component.id.split('_');
    let componentId = compIdTokens[compIdTokens.length - 1];
    let componentName = $("#loc_comp_name_" + componentId).text();
    let compNameTokens = componentName.split(', ');
    compNameTokens.reverse();
    return compNameTokens;
}

function setTabActive(tabName) { // IMPORTANT: Function to support old and new tab-system
    // For example: id="nav-address-search-tab"
    $('#nav-' + tabName + '-tab').trigger('click');
}

function transferComponentTextSearchToHierarchy(component) {

    let compNameTokens = extractComponentTokens(component);
    selectLocation(compNameTokens);

    let address = getSelectedAddressObject();
    json2Table(address, "data_to_transfer");

    $('#form-control-region-select option:contains(' + $('#region').val() + ')').attr('selected', true);
    $('#form-control-region-select').trigger('change');

    showHideAddressTabForm('hierarchical-search-form');

    setTabActive('hierarchical-search');
}
/*---------------------------------------------------
 * Address: Address Tasks
 *--------------------------------------------------*/

function locationsTasksUrl() { // Paneme need siis paika kui Anto APEX-i domeenide lahutamised on ära tehtud

    // return "http://localhost:6548/ress-api/locations/tasks";
    // return "http://ayrton.estpak.ee/api/locations/tasks";

    let origin = window.location.origin;
    let url = null;

    if (origin.indexOf(".elion.ee") >= 0) {
        url = origin + '/rest/resource/ressapi';

    } else {
        url = "https://orumets.ee/telia/api";
        // url = "https://soa-arendus.elion.ee/rest/resource/ressapi";
    }

    return url;
}

function getAddressTasks() {
    showCardPreloader('#tab-address-tasks-content .card');
    $('#tab-address-tasks-content .card:first .card-body').text('');
    $.ajax({
        method: "GET",
        url: locationsTasksUrl() + '/locations/tasks',
        dataType: 'JSON',
        success: function (data) {
            console.log('GET getAddressTasks IS SUCCESS');
            console.log(data);

            setTimeout(function () {
                $('#tab-address-tasks-content .card:first .card-body').text(JSON.stringify(data));
                hideCardPreloader('#tab-address-tasks-content .card');
                showSuccessModal('Õnnestus!', 'Aadressülesanded kenasti laetud')
            }, 1000);
        },
        error: function (data) {
            console.log('GET getAddressTasks HAS ERRORS');
            console.log(data);
        },
        complete: function () {
            console.log('GET getAddressTasks COMPLETED');
        }
    });
}

function formatAddressTasksCommentTypeText(type) {
    let types = {
        athos_task_id: 'Vana Athose ID (andmeülekanne)',
        sender: 'Saatja kommentaar',
        clarification_request: 'Järelpäring',
        cancelled: 'Tühistamine',
    };

    return (types[type]) ? types[type] : 'Kommentaar';
}

function formatAddressTasksCommentTypeColor(type) {
    let types = {
        athos_task_id: 'gray',
        sender: 'blue',
        clarification_request: 'green',
        cancelled: 'red',
    };

    return (types[type]) ? types[type] : 'pink';
}

function formatAddressTasksStatusText(status) {
    let statuses = {
        new: 'Uus',
        in_progress: 'Töös',
        pending_clarification: 'Järelpäringu ootel',
        done: 'Tehtud',
        cancelled: 'Tühistatud',
    };

    return (statuses[status]) ? statuses[status] : 'Puudub';
}

function formatAddressTasksStatusIcon(status) {
    let statuses = {
        new: 'icon-file-blue',
        in_progress: 'icon-processes-blue',
        pending_clarification: 'icon-conversation-blue',
        done: 'icon-check-round-green',
        cancelled: 'icon-close-round-red',
    };

    return (statuses[status]) ? statuses[status] : 'icon-help-blue';
}

function formatAddressTasksDateTimeText(dateString, isOrderable, displaySeconds) {
    let dateTime;

    let date = new Date(dateString);
    let year = date.getFullYear();
    let month = ('0' + (date.getMonth() + 1)).slice(-2);
    let day = ('0' + date.getDate()).slice(-2);
    let hours = ('0' + date.getHours()).slice(-2);
    let minutes = ('0' + date.getMinutes()).slice(-2);
    let seconds = ('0' + date.getSeconds()).slice(-2);

    if (isOrderable) {
        dateTime = year +
            '-' + month +
            '-' + day +
            ' ' + hours +
            ':' + minutes +
            ((displaySeconds) ? ':' + seconds : '');
    } else {
        dateTime = day +
            '.' + month +
            '.' + year +
            ' ' + hours +
            ':' + minutes +
            ((displaySeconds) ? ':' + seconds : '');
    }

    return dateTime;
}

function showAddressTasksTableWithGridJs(searchKeyword) {

    //showCardPreloader('#tab-address-documents-content .card');
    // $('#tab-address-tasks-table').empty();

    $('#tab-address-tasks-table').Grid({
        sort: true,
        resizable: false,

        search: {
            enabled: true,
            keyword: searchKeyword,
            ignoreHiddenColumns: false,
            /* server: {
                url: (prev, keyword) => `${prev}?search=${keyword}`
            }, */
        },
        className: {
            table: 'table table-hover',
            td: 'table-column',
        },
        columns: [{
                name: 'ID',
                sort: true,
                width: '130px',
                hidden: false,
                formatter: (_, row) => gridjs.html(`
                    <a href="javascript:showAddressTaskDetailModal(${row.cells[0].data})" class="d-flex align-items-center font-weight-bold">
                        <i class="icon icon-table icon-new-window"></i>${row.cells[0].data}
                    </a>
                `),
            }, {
                name: 'Staatus',
                formatter: (_, row) => gridjs.html(`
                    <div class="d-flex align-items-center">
                        <i class="icon icon-table-small ${formatAddressTasksStatusIcon(row.cells[1].data)}"></i>
                        <span>${formatAddressTasksStatusText(row.cells[1].data)}</span>
                    </div>
                `),
            },
            {
                name: 'Saatja',
                formatter: (_, row) => gridjs.html(`
                    <span class="d-block">${row.cells[2].data}</span>
                    <a href="mailto:${row.cells[3].data}">${row.cells[3].data}</a>
                `),
            },
            {
                name: 'E-post',
                hidden: true,
            },
            'Kirjeldus',
            {
                name: 'Loodud',
                formatter: (cell) => formatAddressTasksDateTimeText(cell, false, false)
            },
        ],
        pagination: {
            enabled: true,
            limit: 10,
            summary: true,
            /* server: {
                url: (prev, page, limit) => `${prev}?limit=${limit}&offset=${page * limit}`
            } */
        },
        server: {
            method: 'GET',
            url: locationsTasksUrl() + '/locations/tasks',
            headers: {},
            // total: data => data.count,
            then: data => data.map(
                task => [
                    task.task_id,
                    task.status,
                    ifExists(task.sender.name) ? task.sender.name : '',
                    ifExists(task.sender.email) ? task.sender.email : '',
                    ifExists(task.comments[0]) ? task.comments[0].text : '',
                    task.created_time,
                ]
            ),
            handle: (res) => {

                if (res.status === 404) {
                    showErrorModal('Ebaõnnestus!', 'Aadressülesandeid ei leitud');
                    return {
                        data: []
                    };
                }

                if (res.ok || res.status === 200) {
                    // showSuccessModal('Õnnestus!', 'Aadressülesanded on kenasti laetud');
                    return res.json();
                }

                //hideCardPreloader('#tab-address-documents-content .card');

                throw Error('oh no :(');
            },
        },
        language: getGridJsLanguageConfig(),
    });
}

/*---------------------------------------------------
 * Address Task: Attachments
 *--------------------------------------------------*/

function showAddressTaskAttachmentsTableWithGridJs(targetElement, taskAttachmentsJson) {

    $(targetElement).Grid({
        pagination: false,
        sort: true,
        resizable: false,

        className: {
            table: 'table table-esponsive table-hover',
            td: 'table-column',
        },
        columns: [{
            name: 'Fail',
            data: (row) => gridjs.html(`
                <a href="${locationsTasksUrl() + row.url}" class="d-flex align-items-center font-weight-bold">
                    <i class="icon icon-table icon-file icon-file-${row.content_type}" data-toggle="tooltip" title="Disabled tooltip"></i>
                    ${row.file_name}
                </a>
            `),
        }, ],
        data: taskAttachmentsJson,
        language: getGridJsLanguageConfig(),
    }).forceRender();
}

/*---------------------------------------------------
 * Address Task: Comments
 *--------------------------------------------------*/

function getAddressTaskCadastreUrl(cadastre) {
    return `https://xgis.maaamet.ee/ky/${cadastre}`;
}

function getAddressTaskAdsOidUrl(adsoid) {
    return `https://xgis.maaamet.ee/adsavalik/${adsoid}`;
}

function formatAddressTaskCommentText(text) {
    // Replace ADS_OID to links
    let adsOidUrl = getAddressTaskAdsOidUrl('');
    text = text.replace(/(ME|EE|CU)(\d+)/gi, `
        <a href="${adsOidUrl}$&" title="${adsOidUrl}$&" data-toggle="tooltip" target="_blank">$&</a>
    `);

    return text;
}

function showAddressTaskCommentsBubbles(targetElement, taskCommentsJson) {
    taskCommentsJson.forEach((comment) => {
        $(targetElement).append(`
            <div class="speech-bubble speech-bubble-color-${formatAddressTasksCommentTypeColor(comment.type)}">
                ${formatAddressTaskCommentText(comment.text)}
                <div class="speech-bubble-date">${formatAddressTasksDateTimeText(comment.created_time, false, true)}</div>
            </div>
        `);
    });
}

/*---------------------------------------------------
 * Address Task: Map
 *--------------------------------------------------*/

function showAddressTaskLocationOnMap(targetElement, coord_x, coord_y, zoom) {
    let coords = getLatLonFromXY(coord_x, coord_y);

    showOpenLayersMap(targetElement, coords.latitude, coords.longitude, zoom);
}

/*---------------------------------------------------
 * Address Task: Modal
 *--------------------------------------------------*/

function showAddressTaskDetailModal(taskId) {

    // Modal
    let modal = $('#modal-address-task-detail');

    $('.card', modal).each(function () {
        showCardPreloader(this);
    });

    // Reset fields
    $('.dynamic-text', modal).text('-');
    $('.dynamic-text', modal).parent('tr').hide();
    $('.dynamic', modal).html('');
    $('.dynamic-visibility', modal).hide();

    console.log('showAddressTaskDetailModal()');

    // Ajax request
    $.ajax({
        method: "GET",
        url: locationsTasksUrl() + '/locations/tasks?task_id=' + taskId,
        dataType: 'JSON',
        success: function (data) {

            console.info('showAddressTaskDetailModal IS SUCCESS');
            console.log(data);

            if (data[0]) {

                let task = data[0];

                console.log(task);

                // Task ID
                $('.task-id', modal).text('#' + task.task_id);
                $('.task-id', modal).attr('data-original-title',
                    `<div class="d-block text-left">
                        Loodud: <span class="font-weight-bold">${formatAddressTasksDateTimeText(task.created_time, false, true)}</span>
                    </div>
                    <div class="d-block text-left">
                        Looja: <span class="font-weight-bold">${task.created_by}</span>
                    </div>
                `);

                // Task Status
                $('.task-status-text', modal).text(formatAddressTasksStatusText(task.status));
                $('.task-status-icon', modal).removeClass(function (index, className) {
                    return (className.match(/\bicon-\S+/g) || []).join(' ');
                });
                $('.task-status-icon', modal).addClass(formatAddressTasksStatusIcon(task.status));

                $('.task-status', modal).attr('data-original-title',
                    `<div class="d-block text-left">
                        Muudetud: <span class="font-weight-bold">${formatAddressTasksDateTimeText(task.modified_time, false, true)}</span>
                    </div>
                    <div class="d-block text-left">
                        Muutja: <span class="font-weight-bold">${task.modified_by}</span>
                    </div>
                `);

                // Task Location Address
                if (ifExists(task.location.address)) {
                    if (ifExists(task.location.address.region)) $('.task-location-address-region', modal).text(task.location.address.region).parent().show();
                    if (ifExists(task.location.address.municipality)) $('.task-location-address-municipality', modal).text(task.location.address.municipality).parent().show();
                    if (ifExists(task.location.address.city_county)) $('.task-location-address-city-county', modal).text(task.location.address.city_county).parent().show();
                    if (ifExists(task.location.address.street_bg_name)) $('.task-location-address-street-bg-name', modal).text(task.location.address.street_bg_name).parent().show();
                }

                // Task Location Building
                if (ifExists(task.location.building)) {
                    $('.task-location-building-name', modal).text(task.location.building.name).parent().show();

                    if (ifExists(task.location.building.coord_x) && ifExists(task.location.building.coord_y)) {
                        $('.task-location-building-coords', modal).text(task.location.building.coord_x + ', ' + task.location.building.coord_y).parent().show();
                        // Small Map
                        $('.card-map-small', modal).show();
                        showAddressTaskLocationOnMap('modal-address-task-map-content-small', task.location.building.coord_x, task.location.building.coord_y, 17);
                    }
                }

                // Task Location Room
                if (ifExists(task.location.room)) {
                    $('.task-location-room-name', modal).text(task.location.room.name).parent().show();
                }

                if (ifExists(task.cadastre)) $('.task-cadastre', modal).html(`<a href="${getAddressTaskCadastreUrl(task.cadastre)}" title="${getAddressTaskCadastreUrl(task.cadastre)}" data-toggle="tooltip" target="_blank">${task.cadastre}</a>`).parent().show();

                // Task Sender
                if (ifExists(task.sender)) {
                    if (ifExists(task.sender.name)) $('.task-sender-name', modal).text(task.sender.name).parent().show();
                    if (ifExists(task.sender.email)) $('.task-sender-email', modal).html(`<a href="mailto:${task.sender.email}?subject=Aadressülesanne #${task.task_id}">${task.sender.email}</a>`).parent().show();
                    if (ifExists(task.sender.phone)) $('.task-sender-phone', modal).html(`<a href="tel:${task.sender.phone}">${task.sender.phone}</a>`).parent().show();
                }

                // Task Comments
                showAddressTaskCommentsBubbles($('.card-comments .card-body .speech-bubbles', modal), task.comments);

                // Task Attachments
                showAddressTaskAttachmentsTableWithGridJs('#modal-address-tasks-attachments-table', task.attachments);

                // Show Task Modal
                modal.modal('show');

                // Refresh tooltip event
                $('[data-toggle="tooltip"]').tooltip({
                    boundary: 'window',
                    html: true,
                });

            } else {
                showErrorModal('Viga', 'Aadressülesande laadimine ebaõnnestus< br / >või kirje puudub', taskId);
            }
        },
        error: function (data) {

            console.warn('showAddressTaskDetailModal HAS ERRORS');
            console.log(data);

            showErrorModal('Viga', 'Aadressülesande laadimine ebaõnnestus<br />või kirje puudub', taskId);
        },
        complete: function () {

            console.log('showAddressTaskDetailModal COMPLETED');

            // Hide Preloader
            $('.card', modal).each(function () {
                hideCardPreloader(this);
            });
        }
    });
}

/*---------------------------------------------------
 * Fixto: Modal Left Column
 *--------------------------------------------------*/

function initAddressTaskModalLeftColumnFixto() {

    if (getBootstrapBreakpoint() == 'xs' || getBootstrapBreakpoint() == 'sm') return;

    $('#modal-address-task-detail-left-column-fixto').fixTo('#modal-address-task-detail-left-column', {
        top: 25,
        zIndex: 10,
    });
}

/*---------------------------------------------------
 * Init
 *--------------------------------------------------*/

$(document).on('click', '#nav-address-tasks-tab', function () {

    let searchKeyword;

    if (getUrlParam('task_sender_email')) {
        searchKeyword = getUrlParam('task_sender_email');

    } else if (getUrlParam('task_id')) {
        searchKeyword = getUrlParam('task_id');
        showAddressTaskDetailModal(searchKeyword);
    }

    showAddressTasksTableWithGridJs(searchKeyword);
});

$(function () {
    initAddressTaskModalLeftColumnFixto();
});
const API_LEVEL_REGION = 1;
const API_LEVEL_CITY_COUNTY = 2;
const API_LEVEL_MUNICIPALITY = 3;
const API_LEVEL_STREET_FARM = 4;
const API_LEVEL_HOUSE = 5;
const API_LEVEL_ROOM = 6;

let DROP_DOWN_MAP = new Map();
DROP_DOWN_MAP.set('#form-control-region-select', {
    showHide: '#region-drop-down',
    textDataField: '#region',
    childSelect: '#form-control-city-county-select',
    childLevel: API_LEVEL_CITY_COUNTY
});
DROP_DOWN_MAP.set('#form-control-city-county-select', {
    showHide: '#city-county-drop-down',
    textDataField: '#city_county',
    childSelect: '#form-control-municipality-select',
    childLevel: API_LEVEL_MUNICIPALITY
});
DROP_DOWN_MAP.set('#form-control-municipality-select', {
    showHide: '#municipality-drop-down',
    textDataField: '#municipality',
    childSelect: '#form-control-street-select',
    childLevel: API_LEVEL_STREET_FARM
});
DROP_DOWN_MAP.set('#form-control-street-select', {
    showHide: '#street-drop-down',
    textDataField: '#street_bg_name',
    childSelect: '#form-control-house-select',
    childLevel: API_LEVEL_HOUSE
});
DROP_DOWN_MAP.set('#form-control-house-select', {
    showHide: '#house-drop-down',
    textDataField: '#bg_num',
    childSelect: '#form-control-room-select',
    childLevel: API_LEVEL_ROOM
});
DROP_DOWN_MAP.set('#form-control-room-select', {
    showHide: '#room-drop-down',
    textDataField: '#room_name',
    childSelect: null,
    childLevel: null
});
DROP_DOWN_MAP.set('#form-control-anette-room-select', {
    showHide: '#anette-room-drop-down',
    textDataField: '#room_name',
    childSelect: null,
    childLevel: null
});

function addressDropDownsOnChange() {
    DROP_DOWN_MAP.forEach(function (dropDownObject, dropDownId) {
        $(dropDownId).on('change', function (event) {

            let selectedLocCompValue = $(this).find(':selected').val();

            let childLevel = dropDownObject.childLevel;
            if (childLevel == null) {
                let selectedRoom = JSON.parse(selectedLocCompValue);
                selectRoom(selectedRoom);
                return;
            }
            hideAndClearLocCompChildren(childLevel);

            if (selectedLocCompValue != "") {
                if (childLevel < API_LEVEL_ROOM) {
                    queryAndSetLocationComponents(
                        childLevel,
                        dropDownObject.childSelect,
                        selectedLocCompValue,
                        false);
                    // In certain cases we don't have municipality and we need to query streets directly
                    if (childLevel == API_LEVEL_MUNICIPALITY) {
                        let municipalitySelect = DROP_DOWN_MAP.get('#form-control-municipality-select');
                        queryAndSetLocationComponents(
                            municipalitySelect.childLevel,
                            municipalitySelect.childSelect,
                            selectedLocCompValue,
                            true);
                    }
                } else if (childLevel == API_LEVEL_ROOM) {
                    let queryUrlAndParams = getLocationsSearchUrl() + 'id:' + selectedLocCompValue;
                    locationSearchAjaxQuery(queryUrlAndParams, null, getBuildingData);
                }
            }

        });
    });
}

function hideAndClearLocCompChildren(startFromChildLevel) {
    DROP_DOWN_MAP.forEach(function (selectDropDown) {
        if (selectDropDown.childLevel >= startFromChildLevel) {
            $(selectDropDown.childSelect).empty();
            let childSelectDropDown = DROP_DOWN_MAP.get(selectDropDown.childSelect);
            $(childSelectDropDown.showHide).addClass('d-none');
        }
    });
}

function queryAndSetLocationComponents(level,
    selectToUpdate,
    loc_comp_id,
    isMunicipalityEvent) {
    $.ajax({
        url: getGazetteerUrl(),
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        data: {
            level: level,
            id: loc_comp_id
        }
    }).done(function (response) {
        console.log(loc_comp_id, level);
        $(selectToUpdate).empty();
        $(selectToUpdate).append($("<option></option>")
            .attr("value", "")
            .text("- vali -"));
        if (response.length > 0) {
            response.forEach(function (locationComponent) {
                $(selectToUpdate).append($("<option></option>")
                    .attr("value", locationComponent.id)
                    .text(locationComponent.name));
            });
        } else { // TODO: Maybe this is not needed and we can query later when choosing "Vali"
            if (selectToUpdate == '#form-control-house-select') { // Has to be farm
                let addressId = $('#form-control-street-select').val();
                // getBuildingAndChildren(addressId);
                let queryUrlAndParams = getLocationsSearchUrl() + 'id:' + addressId;
                locationSearchAjaxQuery(queryUrlAndParams, null, getBuildingData);
            }

        }
    }).always(function () {
        showDropDownWhenValues(selectToUpdate);
        setAddressCompFromDataTransfer(selectToUpdate, false, isMunicipalityEvent);
        selectComponent(selectToUpdate, DROP_DOWN_MAP);
    });
}

function getBuildingData(searchResponse) {

    let roomSelect = '#form-control-room-select';
    let item = searchResponse.response.docs[0];
    let dataTransferred = false;

    $(roomSelect).empty();
    $(roomSelect).append($("<option></option>")
        .attr("value", "")
        .text("- vali -"));
    let roomsString = item.rooms[0];
    let rooms = JSON.parse(roomsString);
    if (!isEmpty(rooms)) {
        rooms.sort(function (a, b) {
            return a.room_name - b.room_name;
        });
        rooms.forEach(function (room) {
            let roomText = room.room_name + ' ' + room.room_type;
            if (!isEmpty(room.room_entrance)) {
                roomText += ' ' + room.room_entrance + '.trk';
            }
            if (!isEmpty(room.room_floor)) {
                roomText += ' ' + room.room_floor + '.k';
            }
            $(roomSelect).append($("<option></option>")
                .attr("value", JSON.stringify(room))
                // .attr("value", room.room_id + ',' +
                //                room.room_address_id)
                .text(roomText));
        });

        showDropDownWhenValues(roomSelect);
        dataTransferred = setAddressCompFromDataTransfer(roomSelect, true, false);
    }

    let queryAnetteUrlAndParams = getFanAvailabilityAddressesUrl() + item.building_id + '/rooms';
    locationSearchAjaxQuery(queryAnetteUrlAndParams, null, getAnetteBuildingData);

    if (dataTransferred) {
        clearDataToTransfer();
    } else {
        selectAddress(item);
    }
}

function getAnetteBuildingData(searchResponse) {
    let roomSelect = '#form-control-anette-room-select';
    let rooms = searchResponse.results;
    let dataTransferred = false;

    $(roomSelect).empty();
    $(roomSelect).append($("<option></option>")
        .attr("value", "")
        .text("- vali -"));
    if (!isEmpty(rooms)) {
        rooms.sort(function (a, b) {
            return a.room_name - b.room_name;
        });
        rooms.forEach(function (room) {
            let roomText = room.room_name
            $(roomSelect).append($("<option></option>")
                .attr("value", JSON.stringify(room))
                .text(roomText));
        });

        showDropDownWhenValues(roomSelect);
        dataTransferred = setAddressCompFromDataTransfer(roomSelect, true, false);
    }
    if (dataTransferred) {
        clearDataToTransfer();
    }
}

function setAddressCompFromDataTransfer(selectToUpdate,
    isRoomsSelect,
    isMunicipalityEvent) {
    let dropDownObject = DROP_DOWN_MAP.get(selectToUpdate);
    let textDataField = $(dropDownObject.textDataField + '_data_to_transfer').text();
    if (textDataField) {
        selectOptionByText(selectToUpdate, textDataField, isRoomsSelect, isMunicipalityEvent);
        return true;
    }
    return false;
}

function selectOptionByText(selectToUpdate,
    searchedTextValue,
    isRoomSelect,
    isMunicipalityEvent) {
    $(selectToUpdate).find('option').filter(function () {
        let optionTextValue = null;
        optionTextValue = parseOptionTextValue(this, isRoomSelect);
        searchedTextValue = searchedTextValue.trim();
        if (optionTextValue == searchedTextValue) {
            console.log('Selecting: ' + optionTextValue);
        }
        return optionTextValue == searchedTextValue;
    }).attr('selected', true);
    if (!isMunicipalityEvent) { // We need to prevent double onChange events for click
        $(selectToUpdate).trigger('change');
    }
}

function parseOptionTextValue(option, isRoomSelect) {
    if (isRoomSelect) {
        let roomValue = $(option).val();
        if (roomValue) {
            let room = JSON.parse(roomValue);
            return room.room_name;
        }
    }
    return $(option).text().trim();
}

function showDropDownWhenValues(selectToUpdate) {
    if ($(selectToUpdate).children().length > 1) {
        let dropDown = DROP_DOWN_MAP.get(selectToUpdate);
        $(dropDown.showHide).removeClass('d-none');
    }
}
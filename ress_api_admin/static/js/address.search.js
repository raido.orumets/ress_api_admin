const FORM_TABS_ARRAY = [
    'hierarchical-search-form',
    'fulltext-search-form',
    'map-search-form',
    'address-task-form'
];

function getLocationsSearchUrl() { // Paneme need siis paika kui Anto APEX-i domeenide lahutamised on ära tehtud
    // return "http://localhost:6548/ress-api/locations/search?q=";
    // return "http://ayrton.estpak.ee/api/locations/search?q=";
    let origin = window.location.origin;
    let url = null;
    if (origin.indexOf(".elion.ee") >= 0) {
        url = origin + '/rest/resource/ressapi/locations/search?q=';
    } else {
        url = "https://soa-arendus.elion.ee/rest/resource/ressapi/locations/search?q=";
    }
    return url;
}

function getGazetteerUrl() { // Paneme need siis paika kui Anto APEX-i domeenide lahutamised on ära tehtud
    //let url = "http://localhost:6548/ress-api/locations/gazetteer";
    // let url = "http://ayrton.estpak.ee/api/locations/gazetteer";
    let origin = window.location.origin;
    let url = null;
    if (origin.indexOf(".elion.ee") >= 0) {
        url = origin + '/rest/resource/ressapi/locations/gazetteer';
    } else {
        url = "https://soa-arendus.elion.ee/rest/resource/ressapi/locations/gazetteer";
    }
    return url;
}

function getFanAvailabilityAddressesUrl() { // Paneme need siis paika kui Anto APEX-i domeenide lahutamised on ära tehtud
    // return "http://localhost:6548/ress-api/fan/availability/addresses/";
    // return "http://ayrton.estpak.ee/api/fan/availability/addresses/";
    let origin = window.location.origin;
    let url = null;
    if (origin.indexOf(".elion.ee") >= 0) {
        url = origin + '/rest/resource/ressapi/fan/availability/addresses/';
    } else {
        url = "https://soa-arendus.elion.ee/rest/resource/ressapi/fan/availability/addresses/";
    }
    return url;
}

function getGeopankCallbackUrl() { // Paneme need siis paika kui Anto APEX-i domeenide lahutamised on ära tehtud
    let origin = window.location.origin;
    let url = null;
    if (origin.indexOf(".elion.ee") >= 0) {
        url = origin + '/rest/resource/ressapi/locations/address-search/?geopank_params=building_id,address_id';
    } else {
        //url = "https://soa-arendus.elion.ee/rest/resource/ressapi/locations/search-popup/?geopank_params=building_id,address_id";
        url = "http://localhost:8000/address-search/?geopank_params=building_id,address_id";
    }
    return url;
}

$(document).ready(function () {

    // Hierarchical address search
    queryAndSetLocationComponents(level = API_LEVEL_REGION, '#form-control-region-select', null, false);
    addressDropDownsOnChange();

    // Text based search
    installTextSearchAutoComplete();

    // Other click and visibility handlers
    showHideAddressTabForm(null);
    installClickHandlers();

    checkGeopankCallback();
    applyExternalSearchParametersToForm();

});

function selectAddressAndTransferToHierarchy(searchResponse) {
    let item = searchResponse.response.docs[0];
    selectAddress(item);
    transferFullTextSearchToHierarchy();
}

function selectAddressWithRoomAndTransferToHierarchy(searchResponse) {
    let item = searchResponse.response.docs[0];
    selectAddress(item);

    let searchParams = window.location.search;
    let parameterValues = searchParams.slice(searchParams.lastIndexOf(":") + 1).split(',');

    let room_id = parameterValues[9];
    if (room_id) {
        var rooms = JSON.stringify(item.rooms);
        var r_replace = rooms.replace(/\\/g, "");
        r_replace = r_replace.replace(/(\[\")+/g, "");
        r_replace = r_replace.replace(/(\"\])+/g, "");
        rooms = JSON.parse(r_replace);
        selectedRoom = null;
        for (var room_idx in rooms) {
            if (rooms[room_idx].room_id == room_id) {
                selectedRoom = rooms[room_idx];
                console.log('Found room ' + selectedRoom + ' by sent room_id: ' + room_id);
                break;
            }
        }
        if (selectedRoom) {
            selectRoom(selectedRoom);
        }
    }
    transferFullTextSearchToHierarchy();
}

function applyExternalSearchParametersToForm() {
    let searchParams = window.location.search;
    let parameterValues = searchParams.slice(searchParams.lastIndexOf(":") + 1).split(',');
    hasValues = false;
    for (var i = 0; i < parameterValues.length; i++) {
        if (parameterValues[i] !== '') {
            hasValues = true;
            break;
        }
    }

    if (hasValues == false) {
        return;
    }

    let address_id = parameterValues[0];
    let queryUrlAndParams = getLocationsSearchUrl() + 'id:' + address_id;
    locationSearchAjaxQuery(queryUrlAndParams, null, selectAddressWithRoomAndTransferToHierarchy);
}

function checkGeopankCallback() {
    let geopankParams = $.urlParam('geopank_params');
    if (geopankParams) {
        let geopankParamTokens = geopankParams.split(',');
        let buildingId = geopankParamTokens[2];
        let addressId = geopankParamTokens[3];
        let queryUrlAndParams = getLocationsSearchUrl() + 'id:' + addressId +
            '&building_id:' + buildingId;
        locationSearchAjaxQuery(queryUrlAndParams, null, selectAddressAndTransferToHierarchy);
    }
}

function getSelectedAddressObject() {
    var address = new Object();
    address.address_id = $('#address_id').val();
    address.address_type = $('#address_type').val();
    address.region = $('#region').val();
    address.city_county = $('#city_county').val();
    address.municipality = $('#municipality').val();
    address.street_bg_name = $('#street_bg_name').val();
    address.bg_num = $('#bg_num').val();
    address.postal_code = $('#postal_code').val();
    address.building_id = $('#building_id').val();
    address.building_name = $('#building_name').val();
    address.building_subtype = $('#building_subtype').val();
    address.coord_x = $('#coord_x').val();
    address.coord_y = $('#coord_y').val();
    address.room_id = $('#room_id').val();
    address.room_name = $('#room_name').val();
    address.room_type = $('#room_type').val();
    address.room_floor = $('#room_floor').val();
    address.room_entrance = $('#room_entrance').val();
    if (isEmpty($('#room_name').val())) {
        address.room_name = $('#full-text-search-room').val();
    }
    return address;
}

function showHideAddressTabForm(clickedTab) {

    let openTab = $.urlParam('tab');

    if (openTab == false) {
        openTab = 'hierarchical-search-form';
    }

    if (clickedTab != null) {
        openTab = clickedTab;
    }

    if (openTab == 'map-search-form') { // This is needed to not spam Google Maps API
        let origin = window.location.origin;
        let geopankUrl = null;
        if (origin.indexOf("soa-arendus.elion.ee") >= 0) {
            geopankUrl = 'http://geopank-arendus.cc.elion.ee/address/?callback_url=';
        } else if (origin.indexOf("soa-test.elion.ee") >= 0) {
            geopankUrl = 'http://geopank-arendus.cc.elion.ee/address/?callback_url=';
        } else if (origin.indexOf("soa.elion.ee") >= 0) {
            geopankUrl = 'http://geopank.elion.ee/address/?callback_url=';
        } else {
            geopankUrl = 'http://geopank-arendus.cc.elion.ee/address/?callback_url=';
        }

        $('#map-content').html('<iframe class="border-0"' +
            ' src="' + geopankUrl + getGeopankCallbackUrl() +
            '" width="100%"' +
            ' height="600">' +
            ' </iframe>');

    } else {
        $('#map-content').html('');
    }

    FORM_TABS_ARRAY.forEach(function (tabId) {

        let tabFormName = '#' + tabId;
        let tabSelectionButton = tabFormName + '-tab';

        if (openTab == tabId) {
            $(tabFormName).removeClass('d-none');

        } else {
            $(tabFormName).addClass('d-none');
        }

        if (tabSelectionButton.indexOf(openTab) !== -1) {
            $(tabSelectionButton).addClass('active');

        } else {
            $(tabSelectionButton).removeClass('active');
        }

    });

    let isPopup = $.urlParam('popup');
    if (isPopup) {
        $('#navbar').hide();
    }

}

function prepareAddressForTransfer() {
    let addressData = getSelectedAddressObject();

    console.log("Selected address data:");
    console.log(addressData);

    passBack(addressData);
}

function passBack(addressData) {
    if (opener) {
        opener.transferAddr(
            addressData.address_id,
            addressData.address_type,
            addressData.region,
            addressData.city_county,
            addressData.municipality,
            addressData.street_bg_name,
            addressData.bg_num,
            addressData.room_name,
            addressData.postal_code,
            "",
            addressData.building_id,
            addressData.building_name,
            addressData.building_subtype,
            addressData.coord_x,
            addressData.coord_y,
            addressData.room_id,
            addressData.room_floor,
            addressData.room_entrance,
            addressData.room_type,
            "");
        close();
    }
}

function installClickHandlers() {
    FORM_TABS_ARRAY.forEach(function (tabId) {
        let tabSelectionButton = '#' + tabId + '-tab';
        $(tabSelectionButton).click(function (e) {
            showHideAddressTabForm(tabId);
            e.preventDefault();
        });
    });
    $('#fulltext-search-select-btn').click(function (e) {
        transferFullTextSearchToHierarchy();
    });
    $('#hierarchical-search-select-btn').click(function (e) {
        prepareAddressForTransfer();
    });
    $('#component-search-location-btn').click(function (e) {
        queryLocationsByTextComps();
    });
    $('#comp-search-address-radio').click(function (e) {
        $('#component-search-location-box').addClass('d-none');
        $('#component-search-address-box').removeClass('d-none');
    });
    $('#comp-search-location-radio').click(function (e) {
        $('#component-search-address-box').addClass('d-none');
        $('#component-search-location-box').removeClass('d-none');
    });
    $('#add-missing-room-btn').click(function (e) {
        $('#add-missing-room').removeClass('d-none');
    });
}
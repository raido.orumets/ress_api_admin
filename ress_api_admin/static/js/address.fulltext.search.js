function installTextSearchAutoComplete() {
    $("#full-text-search-address").autocomplete({
        source: function (request, response) {
            let queryUrlAndParams = getLocationsSearchUrl() + $("#full-text-search-address").val();
            locationSearchAjaxQuery(queryUrlAndParams, response, null);
        },
        minLength: 3,
        select: function (event, ui) {
            let item = ui.item.item;
            selectAddress(item);
        }
    });
}

function locationSearchAjaxQuery(queryUrlAndParams, autocompleteRespCallback, callback) {
    $.ajax({
        url: queryUrlAndParams,
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            if (autocompleteRespCallback) {
                autocompleteRespCallback($.map(data.response.docs, function (i) {
                    return {
                        label: i.label2,
                        value: i.label2,
                        item: i
                    };
                }));
            } else if (callback) {
                callback(data);
            }
        }
    });
}

function selectComponent(selectToUpdate, DROP_DOWN_MAP) {
    DROP_DOWN_MAP.forEach(function (dropDownObject, dropDownId) {
        if (dropDownObject.childSelect == selectToUpdate) {
            let componentText = $(dropDownId).find(':selected').text()
            let componentVal = $(dropDownId).find(':selected').val();
            console.log('Selecting:', dropDownId, componentText, componentVal);
            if (Boolean(componentVal)) {
                $(dropDownObject.textDataField).val(componentText);
                $('#address_id').val(componentVal);
            }
        }
    });
}

function selectAddress(item) {
    $('#address_id').val(item.id);
    $('#address_type').val(item.address_type);
    $('#region').val(item.region);
    $('#city_county').val(item.city_county);
    $('#municipality').val(item.municipality);
    $('#street_bg_name').val(item.street);
    $('#bg_num').val(item.house);
    $('#postal_code').val(item.postal_code);
    $('#building_id').val(item.building_id);
    $('#building_name').val(item.building_name);
    $('#building_subtype').val(item.building_type);
    $('#coord_x').val(item.building_coord_x);
    $('#coord_y').val(item.building_coord_y);
    console.log("Selected address label: ", item.label2);
    console.log("Selected address JSON object: ", JSON.stringify(item));
    json2Table(item, "selected_address");
    clearDataToTransfer();
    if (!isEmpty(document.getElementById("selected_room"))) {
        document.getElementById("selected_room").outerHTML = "";
    }
    $('#full-text-search-room').val("");
    $('#room_id').val("");
    $('#room_name').val("");
    $('#room_type').val("");
    $('#room_floor').val("");
    $('#room_entrance').val("");
    getRooms(item.rooms);
}

function getRoomLabel(item) {
    return item.room_name + " (" + item.room_street + " " + item.room_house + ")";
}

function selectRoom(item) {
    $('#room_id').val(item.room_id);
    $('#room_name').val(item.room_name);
    $('#room_type').val(item.room_type);
    $('#room_floor').val(item.room_floor);
    $('#room_entrance').val(item.room_entrance);
    $('#address_id').val(item.room_address_id);
    $('#street_bg_name').val(item.room_street);
    $('#bg_num').val(item.room_house);
    log("Selected room label: ", getRoomLabel(item));
    log("Selected room JSON object: ", JSON.stringify(item));
    json2Table(item, "selected_room");
}

function clearDataToTransfer() {
    if (!isEmpty(document.getElementById("data_to_transfer"))) {
        document.getElementById("data_to_transfer").outerHTML = "";
    }
}

function getRooms(data) {
    var rooms = JSON.stringify(data);
    var r_replace = rooms.replace(/\\/g, "");
    r_replace = r_replace.replace(/(\[\")+/g, "");
    r_replace = r_replace.replace(/(\"\])+/g, "");
    rooms = JSON.parse(r_replace);
    log("Building rooms type: ", typeof (rooms));
    log("Building rooms value: ", JSON.stringify(rooms));
    $('#outraw').val(JSON.stringify(rooms));
    $("#full-text-search-room").autocomplete({
        source: function (request, response) {
            var datamap = rooms.map(function (i) {
                return {
                    label: getRoomLabel(i),
                    value: i.room_name,
                    desc: i.room_type,
                    item: i
                }
            });
            var key = request.term;
            datamap = datamap.filter(function (i) {
                return i.value.toLowerCase().indexOf(key.toLowerCase()) >= 0;
            });
            datamap = datamap.sort(function (a, b) {
                return a.value - b.value;
            });
            response(datamap);
            //$('#outraw').val(JSON.stringify(datamap));
        },
        minLength: 0,
        delay: 100,
        select: function (event, ui) {
            let item = ui.item.item;
            selectRoom(item)
        }
    }).focus(function () {
        $(this).autocomplete("search");
    });

}

function json2Table(data, elem) {
    var output = document.getElementById(elem);
    if (isEmpty(output)) {
        $('#outputbox').prepend("<div id=\"" + elem + "\"></div>");
        output = document.getElementById(elem);
    }
    var tr = "";
    for (var k in data) {
        tr += "<tr><td><b>" + k + "</b></td><td id='" + k + "_" + elem + "'>" + data[k] + "</td></tr>";
    }
    output.innerHTML = "<div class=\"tabletitle\">" + elem + ":</div><table class=\"jsondata\"><tr><th class=\"datakey\">key</th><th class=\"dataval\">value</th></tr>" + tr + "</table>";
}

function setTabActive(tabName) {
    // For example: id="nav-address-search-tab"
    $('#nav-' + tabName + '-tab').trigger('click');
}

function transferFullTextSearchToHierarchy() {

    let address = getSelectedAddressObject();
    json2Table(address, "data_to_transfer");

    $('#form-control-region-select option:contains(' + address.region + ')').attr('selected', true);
    $('#form-control-region-select').trigger('change');

    showHideAddressTabForm('hierarchical-search-form');

    setTabActive('hierarchical-search');
}
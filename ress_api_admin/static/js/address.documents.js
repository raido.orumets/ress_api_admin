/*---------------------------------------------------
 * Address: Address Building Documents
 *---------------------------------------------------
 * GeoPank API example:
 *  - Documents list: https://soa-arendus.elion.ee/rest/Resource/geopank/building/doclist/579195
 *  - Document file: https://soa-arendus.elion.ee/rest/Resource/geopank/building/doc/27038
 *--------------------------------------------------*/

function getFileExtension(filename) {
    let extension = filename.substring(0, 1) === '.' ? '' : filename.split('.').slice(1).pop() || '';
    return extension.toLowerCase();
}

function getAddressBuildingDocuments() {
    showCardPreloader('#tab-address-documents-content .card');

    //let table = $('#tab-address-documents-table').detach();
    //let tableRow = $('tbody tr', table).detach();

    $.ajax({
        method: "GET",
        url: "https://orumets.ee/telia/api/locations/documents",
        dataType: 'JSON',
        success: function (data) {
            console.log('GET getAddressBuildingDocuments IS SUCCESS');
            console.log(data);

            setTimeout(function () {

                data.forEach(function (doc) {

                    //let row = $(tableRow).clone();

                    //$('.table-col-name', row).text(doc.name);
                    //$('.table-col-type', row).text(doc.type);
                    //$('.table-col-description', row).text(doc.description);

                    //$('tbody', table).append(row);
                });

                //$('#tab-address-documents-content .card:first .card-body').append(table);

                hideCardPreloader('#tab-address-documents-content .card');
                //showSuccessModal('Õnnestus!', 'Hoone dokumentatsioon kenasti laetud')
            }, 1000);
        },
        error: function (data) {
            console.log('GET getAddressBuildingDocuments HAS ERRORS');
            console.log(data);
        },
        complete: function () {
            console.log('GET getAddressBuildingDocuments COMPLETED');
        }
    });
}

function showAddressBuildingDocumentsTableWithGridJs(buildingId) {

    // TODO: buildingId to API

    //showCardPreloader('#tab-address-documents-content .card');
    //$('#tab-address-documents-table').empty();
    $('#tab-address-documents-table').Grid({
        pagination: false,
        sort: true,
        resizable: false,

        search: {
            enabled: false,
            server: {
                url: (prev, keyword) => `${prev}?search=${keyword}`
            },
            ignoreHiddenColumns: true,
        },
        className: {
            table: 'table table-esponsive table-hover',
            td: 'table-column',
        },
        columns: [{
                name: '#',
                sort: false,
                width: 'auto',
                hidden: true,
                formatter: (cell) => gridjs.html(`<b>${cell}</b>`)
            }, {
                name: 'Dokumendid',
                formatter: (_, row) => gridjs.html(`<a href="https://orumets.ee/telia/api/locations/documents/${row.cells[0].data}/attachment" class="d-flex align-items-center font-weight-bold"><i class="icon icon-table icon-file icon-file-${getFileExtension(row.cells[1].data)}" data-toggle="tooltip" title="Disabled tooltip"></i>${row.cells[1].data}</a>`)
            },
            'Tüüp',
            'Kirjeldus',
        ],
        /* pagination: {
            enabled: true,
            limit: 10,
            summary: false,
            server: {
                url: (prev, page, limit) => `${prev}?limit=${limit}&offset=${page * limit}`
            }
        }, */
        server: {
            method: 'GET',
            url: 'https://orumets.ee/telia/api/locations/documents/' + buildingId,
            headers: {},
            // total: data => data.count,
            then: data => data.map(
                doc => [
                    doc.id,
                    ifExists(doc.name) ? doc.name : '',
                    ifExists(doc.type) ? doc.type : '',
                    ifExists(doc.description) ? doc.description : '',
                ]
            ),
            handle: (res) => {

                if (res.status === 404) {
                    showNotificationModal('icon-file-close-blue', 'Ei leitud', 'Dokumendid antud hoone kohta puuduvad', 'Sulge teade', buildingId);
                    return {
                        data: []
                    };
                }

                if (res.status >= 400 && res.status <= 599) {
                    showErrorModal('Ebaõnnestus', 'Viga andmete pärimisel<br />või hoone dokumentatsiooni ei leitud', buildingId);
                    return {
                        data: []
                    };
                }

                if (res.ok || res.status === 200) {
                    // showSuccessModal('Õnnestus!', 'Hoone dokumentatsioon kenasti laetud');
                    return res.json();
                }

                //hideCardPreloader('#tab-address-documents-content .card');

                throw Error('oh no :(');
            },
        },
        language: getGridJsLanguageConfig(),
    }).forceRender();
}

function getAddressBuildingIdFromUrl() {
    // For example: ?building_id=57919
    return getUrlParam('building_id');
}

/*---------------------------------------------------
 * Init
 *--------------------------------------------------*/

$(document).on('click', '#nav-address-documents-tab', function () {

    // hideOtherTabsExcept('address-documents');

    let buildingId = getAddressBuildingIdFromUrl();
    buildingId = (buildingId) ? buildingId : 57919; // IMPORTANT: This line is for testing only

    showAddressBuildingDocumentsTableWithGridJs(buildingId);
});
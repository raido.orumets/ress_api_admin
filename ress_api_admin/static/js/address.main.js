/*---------------------------------------------------
 * Address: Main
 *--------------------------------------------------*/

var ADMIN_MODE = false;

/*---------------------------------------------------
 * Admin Mode: Set, Get, If
 *--------------------------------------------------*/

function setAdminMode(active) {
    ADMIN_MODE = (active == true) ? true : false;
}

function getAdminMode() {
    return ADMIN_MODE;
}

function ifAdminMode() {
    return getAdminMode();
}

/*---------------------------------------------------
 * Tabs and Navigation
 *--------------------------------------------------*/

function showHideAddressTabForm(tab) {
    // TODO: remove old tab change
}

function setTabActive(tabName) {
    // For example: id="nav-address-search-tab"
    $('#nav-' + tabName + '-tab').trigger('click');
    setHeaderNavLinkActive(tabName);
}

function setHeaderNavLinksInActive() {
    $('.header-nav .header-nav-link').removeClass('active');
}

function setHeaderNavLinkActive(linkName) {
    setHeaderNavLinksInActive();
    $('#header-nav-' + linkName + '-link').addClass('active'); //.trigger('click');
}

function getUrlParams(url) {
    if (!url) {
        url = window.location;
    }

    let urlParams = {};

    new URL(url).searchParams.forEach(function (value, key) {
        if (urlParams[key] !== undefined) {
            if (!Array.isArray(urlParams[key])) {
                urlParams[key] = [urlParams[key]];
            }
            urlParams[key].push(value);
        } else {
            urlParams[key] = value;
        }
    });

    console.log('getUrlParams: ' + JSON.stringify(urlParams));

    return urlParams;
}

function getUrlParam(urlParamName) {
    let urlParams = getUrlParams();

    return ifExists(urlParams[urlParamName]) ? urlParams[urlParamName] : '';
}

function setTabActiveViaUrl() {
    // For example: ?tab=address-search
    let tabName = getUrlParam('tab');

    if (tabName) {
        setTabActive(tabName);
    }
}

function setHeaderBrandUrlToTab(tabName) {
    $('header .header-brand').attr('href', "javascript:setTabActive(' " + tabName + " ')");
}

function hideOtherTabsExcept(tabName) {
    $('#nav-tabs .nav-link').addClass('d-none');
    $('#nav-' + tabName + '-tab').removeClass('d-none').removeClass('d-md-none');
    setHeaderBrandUrlToTab(tabName);
}

/*---------------------------------------------------
 * If object exists
 *--------------------------------------------------*/

function ifExists(object) {
    return (typeof object != 'undefined') ? ((object) ? true : false) : false;
}

/*---------------------------------------------------
 * Popover
 *--------------------------------------------------*/

function closePopoverOnClickOutside() {
    $('body').on('click', function (e) {
        $('[data-toggle=popover]').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
}

/*---------------------------------------------------
 * Textarea: Autogrow
 *--------------------------------------------------*/

function initTextAreaAutoGrow() {
    $('textarea.autogrow').keyup(function (e) {
        while ($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css('borderTopWidth')) + parseFloat($(this).css('borderBottomWidth'))) {
            $(this).height($(this).height() + 1);
        };
    });
}

/*---------------------------------------------------
 * Main tabs: Scroll center
 *--------------------------------------------------*/

function setScrollCenterOnTab(tabElement) {

    var tabs = $('#nav-tabs');
    var activeTab = tabs.find(tabElement);

    var activeTabWidth = activeTab.outerWidth() / 2;
    var position = activeTab.position().left + activeTabWidth;

    var tabsPosition = tabs.scrollLeft();
    var tabsWidth = tabs.outerWidth();

    position = position + tabsPosition - tabsWidth / 2;

    tabs.animate({
        scrollLeft: position
    }, 500, 'linear');

    console.log('Tab position: ' + position);
    console.log('Tab click: ' + $(tabElement).attr('id'));
}

function scrollToTabs() {
    $('html, body').animate({
        scrollTop: $("#nav-tabs").offset().top
    }, 700, 'swing', function () {
        focusTabFirstField();
    });
}

function focusTabFirstField() {
    $('.tab-pane.active.show form input:first').focus();
}

/*---------------------------------------------------
 * jQuery Validation: Defaults and new methods
 *--------------------------------------------------*/

function setValidatorDefaults() {

    $.validator.setDefaults({
        lang: 'et',

        onfocusout: function (e) {
            this.element(e);
        },
        onkeyup: false,
        /* onkeyup: function (e) {
            this.element(e);
        },*/

        focusInvalid: false,
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();

            if (errors) {
                // Scroll to first form element with error
                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top - 110
                }, 800);
                // Focus to first form element with error
                validator.errorList[0].element.focus();
            }
        },

        highlight: function (element) {
            $(element).closest('.form-control').addClass('is-invalid');
        },
        unhighlight: function (element) {
            $(element).closest('.form-control').removeClass('is-invalid');
            $(element).closest('.form-control').addClass('is-valid');
        },

        errorElement: 'div',
        errorClass: 'invalid-feedback',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group-prepend').length) {
                $(element).siblings('.invalid-feedback').append(error);
                //error.insertAfter(element.parent());
            } else if (element.parent('.floating-label').length) {
                $(element).parent().append(error);
            } else {
                error.insertAfter(element);
            }
        },
    });

    $.validator.addMethod(
        "regex",
        function (value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
    );
}

/*---------------------------------------------------
 * Form: Clear fields
 *--------------------------------------------------*/

function clearFormFields(formElement) {
    $(':input', $(formElement))
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected');
}

/*---------------------------------------------------
 * Modals
 *---------------------------------------------------
 * showSuccessModal(false, false);
 * showErrorModal(false, false);
 * showNotificationModal('icon-check-round-green', 'Aitäh', 'Sinu teavitus on saadetud aadressiregistri haldurile', 'Sulge teade');
 *--------------------------------------------------*/

function showNotificationModal(iconClass, title, text, closeText, someErrorId) {

    let modal = $('#modal-notification');
    modal.modal('show');

    $('.modal-icon', modal).removeClass(function (index, className) {
        return (className.match(/\bicon-\S+/g) || []).join(' ');
    });

    if (iconClass) {
        $('.modal-icon', modal).addClass(iconClass);
    } else {
        $('.modal-icon', modal).addClass('icon-alert');
    }
    if (title) $('.modal-title', modal).text(title);
    if (text) $('.modal-text', modal).html(text);
    if (someErrorId) {
        $('.modal-some-error', modal).show();
        if (typeof someErrorId === 'number') {
            $('.modal-some-error-name', modal).text('ID: ');
        } else {
            $('.modal-some-error-name', modal).text('');
        }
        $('.modal-some-error-value', modal).text(someErrorId);
    } else {
        $('.modal-some-error', modal).hide();
    }
    if (closeText) $('.modal-button', modal).text(closeText);
}

function showSuccessModal(title, text) {
    showNotificationModal('icon-check-round-green', (title) ? title : 'Aitäh', (text) ? text : 'Toiming oli edukas', 'Sulge teade');
}

function showErrorModal(title, text, someErrorId) {
    showNotificationModal('icon-close-round-red', (title) ? title : 'Viga', (text) ? text : 'Toiming ebaõnnestus', 'Sulge teade', (someErrorId) ? someErrorId : false);
}

function showFormModal(formElement) {
    let modal = $('#modal-form');
    modal.modal('show');

    $('.modal-body', modal).html($(formElement).parent().html());
}

/*---------------------------------------------------
 * Pop-up window - Fixes dual-screen position, Most browsers, Firefox
 *---------------------------------------------------
 * Usage: openPopUpWindow('http://www.google.com', 'google.com', '1000', '650', {toolbar:1, resizable:1, location:1, menubar:1, status:1});
 *--------------------------------------------------*/

function openPopUpWindow(url, windowName, width, height, opts) {
    var _innerOpts = '';
    if (opts !== null && typeof opts === 'object') {
        for (var p in opts) {
            if (opts.hasOwnProperty(p)) {
                _innerOpts += p + '=' + opts[p] + ',';
            }
        }
    }
    // Fixes dual-screen position, Most browsers, Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var screenWidth = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var screenHeight = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((screenWidth / 2) - (width / 2)) + dualScreenLeft;
    var top = ((screenHeight / 2) - (height / 2)) + dualScreenTop;
    var newWindow = window.open(url, windowName, _innerOpts + ' width=' + width + ', height=' + height + ', top=' + top + ', left=' + left);

    try {
        // Puts focus on the newWindow
        if (window.focus) {
            newWindow.focus();
        }
    } catch (e) {
        // Show error
        showErrorModal('Pop-up blokeeritud', 'Palun lülita oma veebilehitsejas välja pop-up blokeerija.');
    }
}

/*---------------------------------------------------
 * Resize pop-up window
 *---------------------------------------------------*/

function resizeCurrentPopUpWindow(width, height) {

    //setTimeout(function () {
    let left = ((screen.width - width) / 2);
    let top = ((screen.height - height) / 2);

    window.resizeTo(width, height);
    window.moveTo(left, top);
    window.focus();

    displayCurrentWindowDimensions();
}

/*---------------------------------------------------
 * Display Window Dimensions
 *---------------------------------------------------*/
function displayCurrentWindowDimensions() {
    $('#screen-size-indicator .width-and-height').text($(window).width() + 'x' + $(window).height());
    $('#screen-size-indicator .left-and-top').text(window.screenLeft + 'x' + window.screenTop);
}

/*---------------------------------------------------
 * Card: Cancel button
 *--------------------------------------------------*/

function initCardCancelButton() {
    // TODO: Make this code work more easily

    /* $('.btn-cancel').popover({
        placement: 'top',
        trigger: 'manual',
        html: true,
        title: 'Kas katkestan?',
        content: '<p class="popover-text text-justify">Kas oled kindel, et soovid katkestada aadressi teavituse saatmise ja tühjendada lahtrid?</p><div class="text-right"><a href="#" class="btn btn-secondary mr-2">Ei</a><a href="#" class="btn btn-danger">Jah</a></div>',
    }).on('show.bs.popover', function () {
        $("body").append('<div class="popover-backdrop fade show"></div>');
        console.log("popover-overlay: SHOW");
    }).on('hide.bs.popover', function () {
        $(".popover-backdrop").removeClass('show', function () {
            $(this).remove();
        });
        console.log("popover-overlay: REMOVE");
    });

    $('.btn-cancel').click(function (event) {

        $(this).popover('show');

        event.preventDefault();

        //$(this).trigger(event);
    }); */
}

/*---------------------------------------------------
 * Admin Mode: Activate and refresh
 *--------------------------------------------------*/

function refreshAdminMode() {
    if (ifAdminMode()) {
        $('.admin').show('slow');
        console.info('ADMIN MODE: TRUE');
    } else {
        $('.admin').hide('slow');
        console.info('ADMIN MODE: FALSE');
    }
}

function initDropdownItemAdminToggleSwitch() {
    let adminToggleSwitch = $('#dropdown-item-admin-toggle-switch');

    $('.footer-nav-dropdown .dropdown-item-admin').click(function (e) {
        e.stopPropagation();

        if ($(adminToggleSwitch).is(':checked')) {
            $(adminToggleSwitch).prop('checked', false);
            setAdminMode(false);
            console.log('#dropdown-item-admin-toggle-switch: NOT CHECKED');
            // ADD ACTION
        } else {
            $(adminToggleSwitch).prop('checked', true);
            setAdminMode(true);
            console.log('#dropdown-item-admin-toggle-switch: CHECKED');
            // ADD ACTION
        }
        refreshAdminMode();
    });
}

/*---------------------------------------------------
 * DropZone: Default Config
 *--------------------------------------------------*/

function initDropzoneDeafultConfig() {
    Dropzone.prototype.defaultOptions.dictDefaultMessage = "Drop files here to upload";
    Dropzone.prototype.defaultOptions.dictFallbackMessage = "Your browser does not support drag'n'drop file uploads.";
    Dropzone.prototype.defaultOptions.dictFallbackText = "Please use the fallback form below to upload your files like in the olden days.";
    Dropzone.prototype.defaultOptions.dictFileTooBig = "File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.";
    Dropzone.prototype.defaultOptions.dictInvalidFileType = "You can't upload files of this type.";
    Dropzone.prototype.defaultOptions.dictResponseError = "Server responded with {{statusCode}} code.";
    Dropzone.prototype.defaultOptions.dictCancelUpload = "Cancel upload";
    Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "Are you sure you want to cancel this upload?";
    Dropzone.prototype.defaultOptions.dictRemoveFile = "Remove file";
    Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "You can not upload any more files.";
}

/*---------------------------------------------------
 * GridJS: Default Config
 *--------------------------------------------------*/

function getGridJsLanguageConfig() {
    let language = {
        'search': {
            'placeholder': 'Otsitav märksõna...',
        },
        'sort': {
            'sortAsc': 'Järjesta kasvavalt',
            'sortDesc': 'Järjesta kahanevalt',
        },
        'pagination': {
            'previous': 'Eelmine',
            'next': 'Järgmine',
            'navigate': (page, pages) => 'Lehekülg ' + page + ' / ' + pages,
            'page': (page) => 'Lehekülg ' + page,
            'showing': 'Kuvan',
            'of': '- Kokku on',
            'to': 'kuni',
            'results': () => 'kirjet',
        },
        'loading': 'Laadin...',
        'noRecordsFound': 'Andmeid ei leitud',
        'error': 'Andmeid ei leitud', // 'Viga andmete laadimisel',
    };
    return language;
}

/*---------------------------------------------------
 * Fixto
 *--------------------------------------------------*/

function initHeaderFixto() {
    $('header').fixTo('body', {
        zIndex: 10,
    });
}

function initCardFooterFixto() {
    $('.card-footer').fixTo('.card', {
        container: '.card',
        zIndex: 10,
    });
}

function initCardPreloaderSpinnerFixto(cardElement) {
    $('.card-preloader-spinner-container').fixTo(cardElement, {
        zIndex: 9,
    });
}

function initTableFooterFixto() {
    $('.gridjs-footer').fixTo('body', {
        container: '.card',
        zIndex: 10,
    });
}

/*---------------------------------------------------
 * Preloader: Card
 *--------------------------------------------------*/

function showCardPreloader(cardElement) {

    if (!$('.card-preloader', cardElement).length) {

        let html = '<div class="card-preloader">';
        html += '    <div class="card-preloader-spinner-container">';
        html += '        <div class="card-preloader-spinner spinner spinner-border" role="status" aria-hidden="true"></div>';
        html += '        <p class="card-preloader-text">Palun oota...</p>';
        html += '    </div>';
        html += '</div>';

        $(cardElement).prepend(html);

        if ($(cardElement).height() >= $(window).height()) {

            initCardPreloaderSpinnerFixto($('.card-preloader', cardElement));

            $('.card-preloader', cardElement).addClass('card-preloader-fixed');
            let cardFooterHeight = $('.card-footer', cardElement).height();

            $(cardElement).inViewport(function (px) { // `px` represents the amount of visible height
                let fixedSpinnerMarginTop = (px / 2) - cardFooterHeight;
                $('.card-preloader .card-preloader-spinner', cardElement).css('margin-top', fixedSpinnerMarginTop + 'px');
            });
        }
    }
}

function hideCardPreloader(cardElement) {
    $('.card-preloader', cardElement).remove();
}

/*---------------------------------------------------
 * Preloader: Button
 *--------------------------------------------------*/

function showFormButtonPreloader(buttonElement) {

    if (!$('.btn-preloader', buttonElement).length) {

        $(buttonElement).prop('disabled', true).addClass('btn-with-preloader');
        $(buttonElement).children().hide();
        $(buttonElement).prepend('<span class="btn-preloader spinner spinner-border spinner-border-sm mx-4" role="status" aria-hidden="true"></span>');
    }
}

function hideFormButtonPreloader(buttonElement) {
    $(buttonElement).prop('disabled', false).removeClass('btn-with-preloader');
    $('.btn-preloader', buttonElement).remove();
    $(buttonElement).children().show();
}

/*---------------------------------------------------
 * Bootstrap brakepoints
 *--------------------------------------------------*/

function getBootstrapBreakpoint() {

    if (window.innerWidth <= 575.98) {
        // Extra small devices (portrait phones, less than 576px)
        return "xs";
    } else if (window.innerWidth <= 767.98) {
        // Small devices (landscape phones, less than 768px)
        return "sm";
    } else if (window.innerWidth <= 991.98) {
        // Medium devices (tablets, less than 992px)
        return "md";
    } else if (window.innerWidth <= 1199.98) {
        // Large devices (desktops, less than 1200px)
        return "lg";
    } else {
        // Extra large devices (large desktops)
        return "xl";
    }
}

/*---------------------------------------------------
 * Url: Replace braces in URL or string
 *---------------------------------------------------
 * Example:
 * - replaceBraces('https://someurl.ee/api/tasks/{taskId}/file/{file}/download', { taskId: 123, file: 456 });
 * - Result: https://someurl.ee/api/tasks/123/file/456/download
 *--------------------------------------------------*/

function replaceBraces(string, braces) {
    if (typeof braces != 'object') {
        throw new Error("'braces' should be an object.");
    }

    Object.entries(braces).forEach(([key, value]) => {
        string = string.replace(new RegExp('{' + key + '}', 'gi'), value);
    });

    return string;
}

/*---------------------------------------------------
 * Init
 *--------------------------------------------------*/
$(function () {

    resizeCurrentPopUpWindow(1000, 650);

    $(window).resize(function () {
        displayCurrentWindowDimensions();
    });

    setTabActive('address-search');

    setTabActiveViaUrl();

    $('.main-nav-container .nav-tabs .nav-link').click(function () {
        setHeaderNavLinkActive();
    });

    $('body').on('click', '.main-nav-container .nav-tabs .nav-link', function (e) {
        setScrollCenterOnTab($(this));
    });

    $('[data-toggle="tooltip"]').tooltip({
        boundary: 'window',
    });

    $('[data-toggle="dropdown"]').dropdown();

    //$("[data-toggle=popover]").popover();
    //closePopoverOnClickOutside();

    initTextAreaAutoGrow();

    setValidatorDefaults();

    initHeaderFixto();
    initCardFooterFixto();
    initTableFooterFixto();

    initCardCancelButton();

    // initDropzoneDeafultConfig();

    // $('[data-inline-svg]').inlineSvg();

    initDropdownItemAdminToggleSwitch();
    refreshAdminMode();
});
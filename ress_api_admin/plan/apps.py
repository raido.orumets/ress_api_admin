from django.apps import AppConfig


class PlanConfig(AppConfig):
    name = 'ress_api_admin.plan'

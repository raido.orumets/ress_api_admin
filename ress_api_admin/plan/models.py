# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class NwPlan(models.Model):
    # id = models.FloatField()
    name = models.CharField(max_length=500, blank=True, null=True)
    type = models.CharField(max_length=50)
    parent_id = models.FloatField(blank=True, null=True)
    due_date = models.DateField()
    due_date_accuracy = models.CharField(max_length=1)
    interface = models.CharField(max_length=50)
    interface_reference = models.FloatField()
    comments = models.CharField(max_length=4000, blank=True, null=True)
    created_date = models.DateField()
    created_by = models.CharField(max_length=50)
    modified_date = models.DateField()
    modified_by = models.CharField(max_length=50)
    nw_owner_code = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = '"VTSADMIN"."nw_plan"'

    def __str__(self):
        return f'{self.name}, type: {self.type}'


class NwPlanLocTechn(models.Model):
    plt_id = models.FloatField(primary_key=True)
    nw_plan_id = models.FloatField()
    address_id = models.FloatField()
    building_id = models.FloatField()
    room_id = models.FloatField(blank=True, null=True)
    technology = models.CharField(max_length=50)
    te_name_a = models.CharField(max_length=50, blank=True, null=True)
    te_name_z = models.CharField(max_length=100, blank=True, null=True)
    pade_cc_no_z = models.FloatField(blank=True, null=True)
    customer_line_exists = models.CharField(max_length=1, blank=True, null=True)
    inlet_exists = models.CharField(max_length=1, blank=True, null=True)
    processed = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = '"VTSADMIN"."nw_plan_loc_techn"'

from django.contrib import admin
from django.apps import apps
from django.contrib.admin.sites import AlreadyRegistered

from ress_api_admin.plan.models import NwPlanLocTechn, NwPlan


class MultiDBModelAdmin(admin.ModelAdmin):
    using = 'plan'

    def save_model(self, request, obj, form, change):
        obj.save(using=self.using)

    def delete_model(self, request, obj):
        obj.delete(using=self.using)

    def get_queryset(self, request):
        return super().get_queryset(request).using(self.using)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        return super().formfield_for_foreignkey(db_field, request, using=self.using, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        return super().formfield_for_manytomany(db_field, request, using=self.using, **kwargs)


models = apps.get_models()

for model in models:
    try:
        if 'ress_api_admin.plan' in model.__module__:
            model_fields = []
            for field in model._meta.get_fields():
                if field.__module__ == 'django.db.models.fields':
                    model_fields.append(field.name)
            #setattr(MultiDBModelAdmin, 'list_display', tuple(model_fields))
            if model == NwPlan:
                options = {'list_display': ('id',
                                            'name',
                                            'type',
                                            'due_date',
                                            'due_date_accuracy',
                                            'created_date',
                                            'created_by')}
            elif model == NwPlanLocTechn:
                options = {'list_display': ('plt_id',
                                            'nw_plan_id',
                                            'address_id',
                                            'building_id',
                                            'room_id',
                                            'technology')}
            else:
                options = {}
            admin.site.register(model, MultiDBModelAdmin, **options)
    except AlreadyRegistered:
        pass

# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Reservation(models.Model):
    type = models.ForeignKey('Type', models.DO_NOTHING)
    entries_to_create = models.SmallIntegerField()
    created_time = models.DateTimeField()
    modified_time = models.DateTimeField(blank=True, null=True)
    ext_id = models.CharField(max_length=50, blank=True, null=True)
    ext_name = models.CharField(max_length=50)
    ext_username = models.CharField(max_length=50)
    workorder_id = models.CharField(max_length=50, blank=True, null=True)
    workorder_system = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'reservation'

    def __str__(self):
        return f'Type: {self.type}, entries_to_create: {self.entries_to_create},' \
               f' ext_userame: {self.ext_username}, created: {self.created_time}'


class ReservationAttr(models.Model):
    reservation = models.ForeignKey(Reservation, models.DO_NOTHING)
    key = models.CharField(max_length=50)
    value = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'reservation_attr'

    def __str__(self):
        return f'{self.key}: {self.value}'


class Resource(models.Model):
    parent = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)
    name = models.CharField(max_length=50)
    type = models.ForeignKey('Type', models.DO_NOTHING)
    status = models.ForeignKey('Status', models.DO_NOTHING)
    reservation_id = models.IntegerField(blank=True, null=True)
    created_time = models.DateTimeField()
    modified_time = models.DateTimeField(blank=True, null=True)
    created_ext_id = models.CharField(max_length=50, blank=True, null=True)
    created_ext_name = models.CharField(max_length=50)
    created_ext_username = models.CharField(max_length=50)
    modified_ext_id = models.CharField(max_length=50, blank=True, null=True)
    modified_ext_name = models.CharField(max_length=50, blank=True, null=True)
    modified_ext_username = models.CharField(max_length=50, blank=True, null=True)
    name_char = models.CharField(max_length=50, blank=True, null=True)
    name_number = models.IntegerField(blank=True, null=True)
    workorder_id = models.CharField(max_length=50, blank=True, null=True)
    workorder_system = models.CharField(max_length=50, blank=True, null=True)

    # A unique constraint could not be introspected.
    class Meta:
        managed = False
        db_table = 'resource'

    def __str__(self):
        return self.name


class ResourceLog(models.Model):
    resource_id = models.IntegerField()
    resource_parent_id = models.IntegerField(blank=True, null=True)
    resource_name = models.CharField(max_length=50)
    type_name = models.CharField(max_length=50)
    status_name = models.CharField(max_length=50)
    logged_time = models.DateTimeField()
    created_ext_id = models.CharField(max_length=50, blank=True, null=True)
    created_ext_name = models.CharField(max_length=50)
    created_ext_username = models.CharField(max_length=50)
    modified_ext_id = models.CharField(max_length=50, blank=True, null=True)
    modified_ext_name = models.CharField(max_length=50, blank=True, null=True)
    modified_ext_username = models.CharField(max_length=50, blank=True, null=True)
    action = models.CharField(max_length=50)
    workorder_id = models.CharField(max_length=50, blank=True, null=True)
    workorder_system = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'resource_log'

    def __str(self):
        return self.resource_name


class Status(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=50, blank=True, null=True)
    to_status = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'status'

    def __str__(self):
        return self.name


class Type(models.Model):
    parent_id = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=50)
    resource_name_rule = models.CharField(max_length=50, blank=True, null=True)
    resource_parent_mandatory = models.BooleanField(blank=True, null=True)
    group = models.CharField(max_length=50, blank=True, null=True)
    resource_attrs_mandatory = models.CharField(max_length=100, blank=True, null=True)
    resource_is_addable = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'type'

    def __str__(self):
        return self.name

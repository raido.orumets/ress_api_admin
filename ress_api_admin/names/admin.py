from django.contrib import admin
from django.apps import apps
from django.contrib.admin.sites import AlreadyRegistered

from ress_api_admin.names.models import Resource


class MultiDBModelAdmin(admin.ModelAdmin):
    using = 'names'

    def save_model(self, request, obj, form, change):
        obj.save(using=self.using)

    def delete_model(self, request, obj):
        obj.delete(using=self.using)

    def get_queryset(self, request):
        return super().get_queryset(request).using(self.using)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        return super().formfield_for_foreignkey(db_field, request, using=self.using, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        return super().formfield_for_manytomany(db_field, request, using=self.using, **kwargs)


models = apps.get_models()

for model in models:
    try:
        if 'ress_api_admin.names' in model.__module__:
            options = {}
            model_fields = []
            for field in model._meta.get_fields():
                if field.__module__ == 'django.db.models.fields':
                    if field.name is not 'id':
                        model_fields.append(field.name)
#            options['fields'] = model_fields
            if model == Resource:
                options['list_display'] = ('id',
                                           'name',
                                           'type',
                                           'status',
                                           'created_time',
                                           'created_ext_name')
                options['search_fields'] = ('name', 'created_ext_name')
                options['autocomplete_fields'] = ('parent',)
                options['list_filter'] = ('status', 'type', 'created_time')
            else:
                options = {}
            admin.site.register(model, MultiDBModelAdmin, **options)
    except AlreadyRegistered:
        pass

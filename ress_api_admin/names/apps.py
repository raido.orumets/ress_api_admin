from django.apps import AppConfig


class NamesConfig(AppConfig):
    name = 'ress_api_admin.names'

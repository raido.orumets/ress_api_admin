from ress_api_admin.names.models import Reservation


class RessAPIDbRouter:
    def db_for_read(self, model, **hints):
        if model.__module__ == 'ress_api_admin.names.models':
            print('Routing to names for read')
            return 'names'
        return None

    def db_for_write(self, model, **hints):
        if model.__module__ == 'ress_api_admin.names.models':
            print('Routing to names for write')
            return 'names'
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label == 'names':
            return False
        else:
            return True

from django.apps import AppConfig


class LocationsConfig(AppConfig):
    name = 'ress_api_admin.locations'

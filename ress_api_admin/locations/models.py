# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AadressArh(models.Model):
    archive_id = models.AutoField(primary_key=True)
    id = models.IntegerField()
    maja = models.CharField(max_length=50, blank=True, null=True)
    postcode = models.CharField(max_length=5, blank=True, null=True)
    tanav_id = models.IntegerField(blank=True, null=True)
    tanav = models.CharField(max_length=50, blank=True, null=True)
    asum_id = models.IntegerField(blank=True, null=True)
    asum = models.CharField(max_length=25, blank=True, null=True)
    linnaosa_id = models.IntegerField(blank=True, null=True)
    linnaosa = models.CharField(max_length=25, blank=True, null=True)
    asula_id = models.IntegerField(blank=True, null=True)
    asula = models.CharField(max_length=25)
    vald_id = models.IntegerField(blank=True, null=True)
    vald = models.CharField(max_length=25, blank=True, null=True)
    maakond_id = models.IntegerField(blank=True, null=True)
    maakond = models.CharField(max_length=25)
    created = models.DateTimeField()
    created_by = models.CharField(max_length=12)
    changed = models.DateTimeField()
    changed_by = models.CharField(max_length=12)
    change_reason = models.CharField(max_length=16)
    change_comment = models.CharField(max_length=256, blank=True, null=True)
    archived = models.DateTimeField()
    archived_by = models.CharField(max_length=12)
    valid_from = models.DateTimeField(blank=True, null=True)
    valid_until = models.DateTimeField(blank=True, null=True)
    hg_kood = models.CharField(max_length=8, blank=True, null=True)
    ath_adr_id = models.CharField(max_length=9, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'aadress_arh'


class AddrChanged(models.Model):
    addr_changed_id = models.AutoField(primary_key=True)
    address_id = models.IntegerField(blank=True, null=True)
    address_arh_id = models.IntegerField(blank=True, null=True)
    processed = models.CharField(max_length=1, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=30, blank=True, null=True)
    id_type = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'addr_changed'


class AddrLocCompRel(models.Model):
    loc_component_id = models.IntegerField(primary_key=True)
    address_id = models.IntegerField()
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'addr_loc_comp_rel'
        unique_together = (('loc_component_id', 'address_id'),)


class AddrLocCompRelB17(models.Model):
    loc_component_id = models.IntegerField()
    address_id = models.IntegerField()
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'addr_loc_comp_rel_b17'


class AddrRel(models.Model):
    address_id = models.IntegerField(primary_key=True)
    rel_address_id = models.IntegerField()
    valid_from = models.DateTimeField()
    valid_to = models.DateTimeField(blank=True, null=True)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    rel_type = models.CharField(max_length=1)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'addr_rel'
        unique_together = (('address_id', 'rel_address_id'),)


class AddrRelArh(models.Model):
    address_id = models.IntegerField()
    rel_address_id = models.IntegerField()
    valid_from = models.DateTimeField()
    valid_to = models.DateTimeField(blank=True, null=True)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    rel_type = models.CharField(max_length=1)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)
    date_archived = models.DateTimeField()
    archived_by = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'addr_rel_arh'


class AddrRelB17(models.Model):
    address_id = models.IntegerField()
    rel_address_id = models.IntegerField()
    valid_from = models.DateTimeField()
    valid_to = models.DateTimeField(blank=True, null=True)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    rel_type = models.CharField(max_length=1)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'addr_rel_b17'


class AddrRelRule(models.Model):
    addr_rel_rule_id = models.AutoField(primary_key=True)
    address_type = models.CharField(max_length=10)
    rel_address_type = models.CharField(max_length=10)
    rel_type = models.CharField(max_length=1)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'addr_rel_rule'


class AddrTaskFiles(models.Model):
    atf_pk = models.IntegerField(blank=True, null=True)
    at_pk = models.CharField(max_length=10, blank=True, null=True)
    manus = models.BinaryField(blank=True, null=True)
    faili_nimi = models.CharField(max_length=50, blank=True, null=True)
    mime_tyyp = models.CharField(max_length=50, blank=True, null=True)
    faili_pikkus = models.IntegerField(blank=True, null=True)
    faili_lisaja = models.CharField(max_length=150, blank=True, null=True)
    faili_lisamise_aeg = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'addr_task_files'


class AddrTasks(models.Model):
    too_kood = models.CharField(primary_key=True, max_length=10)
    maakond_id = models.IntegerField(blank=True, null=True)
    maakond = models.CharField(max_length=80, blank=True, null=True)
    vald_id = models.IntegerField(blank=True, null=True)
    vald = models.CharField(max_length=80, blank=True, null=True)
    asula_id = models.IntegerField(blank=True, null=True)
    asula = models.CharField(max_length=80, blank=True, null=True)
    linnaosa_id = models.IntegerField(blank=True, null=True)
    linnaosa = models.CharField(max_length=80, blank=True, null=True)
    osavald_id = models.IntegerField(blank=True, null=True)
    osavald = models.CharField(max_length=80, blank=True, null=True)
    asum_id = models.IntegerField(blank=True, null=True)
    asum = models.CharField(max_length=80, blank=True, null=True)
    tanav_id = models.IntegerField(blank=True, null=True)
    tanav = models.CharField(max_length=80, blank=True, null=True)
    hg_kood = models.CharField(max_length=10, blank=True, null=True)
    hoone_id = models.IntegerField(blank=True, null=True)
    hoone = models.CharField(max_length=80, blank=True, null=True)
    ruum = models.CharField(max_length=20, blank=True, null=True)
    aadress_id = models.IntegerField(blank=True, null=True)
    katastritunnus = models.CharField(max_length=30, blank=True, null=True)
    staatus = models.CharField(max_length=20, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    changed_by = models.CharField(max_length=50, blank=True, null=True)
    vastuse_prognoositud_aeg = models.DateTimeField(blank=True, null=True)
    date_changed = models.DateTimeField(blank=True, null=True)
    lisainfo = models.CharField(max_length=1000, blank=True, null=True)
    tyhistamise_pohjus = models.CharField(max_length=500, blank=True, null=True)
    paringu_vastus = models.CharField(max_length=1000, blank=True, null=True)
    toote_nr = models.CharField(max_length=50, blank=True, null=True)
    klient = models.CharField(max_length=80, blank=True, null=True)
    saatja_isik = models.CharField(max_length=80, blank=True, null=True)
    saatja_email = models.CharField(max_length=200, blank=True, null=True)
    saatja_telefon = models.CharField(max_length=50, blank=True, null=True)
    muu_info_saatjalt = models.CharField(max_length=1000, blank=True, null=True)
    jarelparingu_saaja = models.CharField(max_length=200, blank=True, null=True)
    x_koord = models.IntegerField(blank=True, null=True)
    y_koord = models.IntegerField(blank=True, null=True)
    kirja_sisu = models.CharField(max_length=1000, blank=True, null=True)
    maja = models.CharField(max_length=200, blank=True, null=True)
    tagasiside = models.CharField(max_length=1, blank=True, null=True)
    haldaja = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'addr_tasks'


class Address(models.Model):
    address_id = models.AutoField(primary_key=True)
    address_type = models.CharField(max_length=10)
    addr_string1 = models.CharField(max_length=500, blank=True, null=True)
    addr_string2 = models.CharField(max_length=500, blank=True, null=True)
    postal_code = models.CharField(max_length=5, blank=True, null=True)
    region_id = models.IntegerField(blank=True, null=True)
    region = models.CharField(max_length=80, blank=True, null=True)
    city_county_id = models.IntegerField(blank=True, null=True)
    city_county = models.CharField(max_length=80, blank=True, null=True)
    municipality_id = models.IntegerField(blank=True, null=True)
    municipality = models.CharField(max_length=80, blank=True, null=True)
    street_bg_name_id = models.IntegerField(blank=True, null=True)
    street_bg_name = models.CharField(max_length=80, blank=True, null=True)
    bg_num_id = models.IntegerField(blank=True, null=True)
    bg_num = models.CharField(max_length=50, blank=True, null=True)
    add_information = models.CharField(max_length=1000, blank=True, null=True)
    status = models.CharField(max_length=1)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    rel_addr_flag = models.CharField(max_length=1, blank=True, null=True)
    hg_kood = models.CharField(max_length=10, blank=True, null=True)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)
    fulladdress = models.CharField(max_length=4000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'address'

    def __str__(self):
        addr_components = [self.bg_num, self.street_bg_name, self.municipality, self.city_county, self.region]
        addr_components = [comp for comp in addr_components if comp]
        return ', '.join(addr_components)


class AddressArh(models.Model):
    address_id = models.IntegerField()
    address_type = models.CharField(max_length=10)
    addr_string1 = models.CharField(max_length=500, blank=True, null=True)
    addr_string2 = models.CharField(max_length=500, blank=True, null=True)
    postal_code = models.CharField(max_length=5, blank=True, null=True)
    region_id = models.IntegerField(blank=True, null=True)
    region = models.CharField(max_length=80, blank=True, null=True)
    city_county_id = models.IntegerField(blank=True, null=True)
    city_county = models.CharField(max_length=80, blank=True, null=True)
    municipality_id = models.IntegerField(blank=True, null=True)
    municipality = models.CharField(max_length=80, blank=True, null=True)
    street_bg_name_id = models.IntegerField(blank=True, null=True)
    street_bg_name = models.CharField(max_length=80, blank=True, null=True)
    bg_num_id = models.IntegerField(blank=True, null=True)
    bg_num = models.CharField(max_length=50, blank=True, null=True)
    add_information = models.CharField(max_length=1000, blank=True, null=True)
    status = models.CharField(max_length=1)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    rel_addr_flag = models.CharField(max_length=1, blank=True, null=True)
    hg_kood = models.CharField(max_length=10, blank=True, null=True)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)
    date_archived = models.DateTimeField()
    archived_by = models.CharField(max_length=30)
    address_arh_id = models.AutoField(primary_key=True)
    action = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'address_arh'


class AddressB17(models.Model):
    address_id = models.IntegerField()
    address_type = models.CharField(max_length=10)
    addr_string1 = models.CharField(max_length=500, blank=True, null=True)
    addr_string2 = models.CharField(max_length=500, blank=True, null=True)
    postal_code = models.CharField(max_length=5, blank=True, null=True)
    region_id = models.IntegerField(blank=True, null=True)
    region = models.CharField(max_length=80, blank=True, null=True)
    city_county_id = models.IntegerField(blank=True, null=True)
    city_county = models.CharField(max_length=80, blank=True, null=True)
    municipality_id = models.IntegerField(blank=True, null=True)
    municipality = models.CharField(max_length=80, blank=True, null=True)
    street_bg_name_id = models.IntegerField(blank=True, null=True)
    street_bg_name = models.CharField(max_length=80, blank=True, null=True)
    bg_num_id = models.IntegerField(blank=True, null=True)
    bg_num = models.CharField(max_length=50, blank=True, null=True)
    add_information = models.CharField(max_length=1000, blank=True, null=True)
    status = models.CharField(max_length=1)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    rel_addr_flag = models.CharField(max_length=1, blank=True, null=True)
    hg_kood = models.CharField(max_length=10, blank=True, null=True)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)
    fulladdress = models.CharField(max_length=4000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'address_b17'


class AddressType(models.Model):
    address_type = models.CharField(primary_key=True, max_length=10)
    addr_type_name = models.CharField(max_length=100)
    description = models.CharField(max_length=150, blank=True, null=True)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'address_type'


class ArgosAddress(models.Model):
    aar_id = models.IntegerField(blank=True, null=True)
    aar_code = models.CharField(max_length=100, blank=True, null=True)
    country = models.CharField(max_length=100, blank=True, null=True)
    address_id = models.IntegerField(blank=True, null=True)
    addr_id_type = models.CharField(max_length=100, blank=True, null=True)
    region = models.CharField(max_length=100, blank=True, null=True)
    city_county = models.CharField(max_length=100, blank=True, null=True)
    municipality = models.CharField(max_length=100, blank=True, null=True)
    street_bg_name = models.CharField(max_length=100, blank=True, null=True)
    adr_bg_num = models.CharField(max_length=100, blank=True, null=True)
    postal_code = models.CharField(max_length=100, blank=True, null=True)
    building_id = models.IntegerField(blank=True, null=True)
    building_name = models.CharField(max_length=100, blank=True, null=True)
    building_subtype = models.CharField(max_length=100, blank=True, null=True)
    coord_x = models.IntegerField(blank=True, null=True)
    coord_y = models.IntegerField(blank=True, null=True)
    room_id = models.IntegerField(blank=True, null=True)
    room_name = models.CharField(max_length=100, blank=True, null=True)
    floor = models.CharField(max_length=100, blank=True, null=True)
    entrance = models.CharField(max_length=100, blank=True, null=True)
    room_type = models.CharField(max_length=100, blank=True, null=True)
    add_information = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'argos_address'


class ArhAnalyseLog(models.Model):
    arh_analyse_log_id = models.AutoField(primary_key=True)
    table_name = models.CharField(max_length=11, blank=True, null=True)
    archive_id = models.IntegerField(blank=True, null=True)
    rec_arh_id = models.IntegerField(blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'arh_analyse_log'


class BObject(models.Model):
    b_object_id = models.AutoField(primary_key=True)
    bo_name = models.CharField(max_length=100, blank=True, null=True)
    bo_description = models.CharField(max_length=1000, blank=True, null=True)
    loc_component_id = models.IntegerField(blank=True, null=True)
    bo_type = models.CharField(max_length=10)
    bo_coord_x = models.IntegerField(blank=True, null=True)
    bo_coord_y = models.IntegerField(blank=True, null=True)
    bo_alt_name = models.CharField(max_length=100, blank=True, null=True)
    no_of_flats = models.IntegerField(blank=True, null=True)
    no_of_floors = models.IntegerField(blank=True, null=True)
    no_of_entrances = models.IntegerField(blank=True, null=True)
    add_information = models.CharField(max_length=1000, blank=True, null=True)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    bo_subtype = models.CharField(max_length=10, blank=True, null=True)
    old_code = models.CharField(unique=True, max_length=10, blank=True, null=True)
    hg_kood = models.CharField(max_length=10, blank=True, null=True)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)
    no_of_nonflats = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'b_object'


class BObjectArh(models.Model):
    b_object_id = models.IntegerField()
    bo_name = models.CharField(max_length=100, blank=True, null=True)
    bo_description = models.CharField(max_length=1000, blank=True, null=True)
    loc_component_id = models.IntegerField(blank=True, null=True)
    bo_type = models.CharField(max_length=10)
    bo_coord_x = models.IntegerField(blank=True, null=True)
    bo_coord_y = models.IntegerField(blank=True, null=True)
    bo_alt_name = models.CharField(max_length=100, blank=True, null=True)
    no_of_flats = models.IntegerField(blank=True, null=True)
    no_of_floors = models.IntegerField(blank=True, null=True)
    no_of_entrances = models.IntegerField(blank=True, null=True)
    add_information = models.CharField(max_length=1000, blank=True, null=True)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    bo_subtype = models.CharField(max_length=10, blank=True, null=True)
    old_code = models.CharField(max_length=10, blank=True, null=True)
    hg_kood = models.CharField(max_length=10, blank=True, null=True)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)
    date_archived = models.DateTimeField()
    archived_by = models.CharField(max_length=30)
    b_object_arh_id = models.IntegerField(blank=True, null=True)
    action = models.CharField(max_length=1, blank=True, null=True)
    no_of_nonflats = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'b_object_arh'


class BObjectB17(models.Model):
    b_object_id = models.IntegerField()
    bo_name = models.CharField(max_length=100, blank=True, null=True)
    bo_description = models.CharField(max_length=1000, blank=True, null=True)
    loc_component_id = models.IntegerField(blank=True, null=True)
    bo_type = models.CharField(max_length=10)
    bo_coord_x = models.IntegerField(blank=True, null=True)
    bo_coord_y = models.IntegerField(blank=True, null=True)
    bo_alt_name = models.CharField(max_length=100, blank=True, null=True)
    no_of_flats = models.IntegerField(blank=True, null=True)
    no_of_floors = models.IntegerField(blank=True, null=True)
    no_of_entrances = models.IntegerField(blank=True, null=True)
    add_information = models.CharField(max_length=1000, blank=True, null=True)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    bo_subtype = models.CharField(max_length=10, blank=True, null=True)
    old_code = models.CharField(max_length=10, blank=True, null=True)
    hg_kood = models.CharField(max_length=10, blank=True, null=True)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'b_object_b17'


class BObjectCadRel(models.Model):
    bo_cad_rel_id = models.AutoField(primary_key=True)
    b_object_id = models.IntegerField()
    cadaster_id = models.IntegerField()
    valid_from = models.DateTimeField()
    valid_to = models.DateTimeField(blank=True, null=True)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    last_change_reason = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'b_object_cad_rel'


class BObjectChanged(models.Model):
    b_object_changed_id = models.AutoField(primary_key=True)
    b_object_id = models.IntegerField(blank=True, null=True)
    b_object_arh_id = models.IntegerField(blank=True, null=True)
    processed = models.CharField(max_length=1, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=30, blank=True, null=True)
    id_type = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'b_object_changed'


class BObjectSubtype(models.Model):
    bo_subtype = models.CharField(primary_key=True, max_length=10)
    bo_type = models.CharField(max_length=10)
    bo_subtype_name = models.CharField(max_length=100)
    status = models.CharField(max_length=1, blank=True, null=True)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'b_object_subtype'


class BObjectType(models.Model):
    bo_type = models.CharField(primary_key=True, max_length=10)
    bo_type_name = models.CharField(max_length=100)
    bo_source = models.CharField(max_length=10, blank=True, null=True)
    status = models.CharField(max_length=1, blank=True, null=True)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'b_object_type'


class BoTypeLctRel(models.Model):
    bo_type_lct_rel_id = models.AutoField(primary_key=True)
    bo_type = models.CharField(max_length=10)
    loc_comp_type = models.CharField(max_length=10)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'bo_type_lct_rel'


class Cadaster(models.Model):
    cadaster_id = models.AutoField(primary_key=True)
    cadaster_name = models.CharField(max_length=500, blank=True, null=True)
    cadaster_code = models.CharField(max_length=30)
    valid_from = models.DateTimeField()
    valid_to = models.DateTimeField(blank=True, null=True)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    last_change_reason = models.CharField(max_length=1000, blank=True, null=True)
    loc_component_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cadaster'


class ChangedAddrActAddr(models.Model):
    changed_addr_act_addr_id = models.AutoField(primary_key=True)
    changed_addr_all_hg_id = models.IntegerField(blank=True, null=True)
    address_id = models.IntegerField(blank=True, null=True)
    hg_kood = models.CharField(max_length=8, blank=True, null=True)
    addr_string1 = models.CharField(max_length=1000, blank=True, null=True)
    addr_string2 = models.CharField(max_length=1000, blank=True, null=True)
    approved = models.CharField(max_length=1, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'changed_addr_act_addr'


class ChangedAddrAllHg(models.Model):
    changed_addr_all_hg_id = models.AutoField(primary_key=True)
    changed_addr_hg_id = models.IntegerField(blank=True, null=True)
    hg_kood = models.CharField(max_length=8, blank=True, null=True)
    address_id = models.IntegerField(blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'changed_addr_all_hg'


class ChangedAddrHg(models.Model):
    changed_addr_hg_id = models.AutoField(primary_key=True)
    address_id = models.IntegerField(blank=True, null=True)
    last_hg_kood = models.CharField(max_length=10, blank=True, null=True)
    addr_string = models.CharField(max_length=1000, blank=True, null=True)
    archive_id = models.IntegerField(blank=True, null=True)
    processed = models.CharField(max_length=1, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=30, blank=True, null=True)
    session_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'changed_addr_hg'


class ChangedAddrView(models.Model):
    old_address_id = models.IntegerField(blank=True, null=True)
    old_hg_kood = models.CharField(max_length=10, blank=True, null=True)
    id_type = models.CharField(max_length=1, blank=True, null=True)
    new_hg_kood = models.CharField(max_length=10, blank=True, null=True)
    new_address_id = models.IntegerField(blank=True, null=True)
    new_addr_string = models.CharField(max_length=500, blank=True, null=True)
    new_region = models.CharField(max_length=80, blank=True, null=True)
    new_city_county = models.CharField(max_length=80, blank=True, null=True)
    new_municipality = models.CharField(max_length=80, blank=True, null=True)
    new_street_bg_name = models.CharField(max_length=80, blank=True, null=True)
    new_bg_num = models.CharField(max_length=50, blank=True, null=True)
    new_postal_code = models.CharField(max_length=5, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'changed_addr_view'


class CompAltRel(models.Model):
    comp_alt_rel_id = models.AutoField(primary_key=True)
    loc_component_id = models.IntegerField()
    rel_component_id = models.IntegerField()
    valid_from = models.DateTimeField(blank=True, null=True)
    rel_type = models.CharField(max_length=1, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=30, blank=True, null=True)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'comp_alt_rel'


class CompAltRelArh(models.Model):
    comp_alt_rel_id = models.IntegerField()
    loc_component_id = models.IntegerField()
    rel_component_id = models.IntegerField()
    valid_from = models.DateTimeField(blank=True, null=True)
    rel_type = models.CharField(max_length=1, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=30, blank=True, null=True)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'comp_alt_rel_arh'


class CompArhRel(models.Model):
    comp_arh_rel_id = models.AutoField(primary_key=True)
    old_component_id = models.IntegerField()
    new_component_id = models.IntegerField()
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'comp_arh_rel'


class ElionSystem(models.Model):
    elion_system_id = models.AutoField(primary_key=True)
    system_name = models.CharField(max_length=30, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'elion_system'


class Err(models.Model):
    err_id = models.AutoField(primary_key=True)
    text_internal = models.CharField(max_length=4000, blank=True, null=True)
    lang1 = models.CharField(max_length=4000, blank=True, null=True)
    lang2 = models.CharField(max_length=4000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'err'


class ErrorMsgs(models.Model):
    error_code = models.IntegerField(primary_key=True)
    error_msg = models.CharField(max_length=512, blank=True, null=True)
    app_id = models.CharField(max_length=10)
    language = models.CharField(max_length=2)

    class Meta:
        managed = False
        db_table = 'error_msgs'
        unique_together = (('error_code', 'app_id', 'language'),)


class EtakRelationship(models.Model):
    b_object_id = models.IntegerField(primary_key=True)
    adob_id = models.IntegerField()
    etak_id = models.IntegerField(blank=True, null=True)
    ads_oid = models.CharField(max_length=12, blank=True, null=True)
    pni_id = models.IntegerField(blank=True, null=True)
    hinne = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'etak_relationship'
        unique_together = (('b_object_id', 'adob_id'),)


class GisAthRel(models.Model):
    loc_component_id = models.IntegerField(primary_key=True)
    old_code = models.CharField(max_length=18)
    gis_id = models.IntegerField(blank=True, null=True)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'gis_ath_rel'
        unique_together = (('loc_component_id', 'old_code'),)


class GisAthRelArh(models.Model):
    gis_ath_rel_id = models.IntegerField()
    loc_component_id = models.IntegerField()
    old_code = models.CharField(max_length=18, blank=True, null=True)
    gis_id = models.IntegerField(blank=True, null=True)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'gis_ath_rel_arh'


class HooneArh(models.Model):
    archive_id = models.AutoField(primary_key=True)
    id = models.IntegerField()
    hoonetyyp = models.CharField(max_length=10)
    selgitus = models.CharField(max_length=60, blank=True, null=True)
    korruseid = models.IntegerField(blank=True, null=True)
    trepikodasid = models.IntegerField(blank=True, null=True)
    kortereid = models.IntegerField(blank=True, null=True)
    add_information = models.CharField(max_length=256, blank=True, null=True)
    aadress_id = models.IntegerField()
    ath_h_kood = models.CharField(max_length=9)
    hg_kood = models.CharField(max_length=8)
    valid_from = models.DateTimeField(blank=True, null=True)
    valid_until = models.DateTimeField(blank=True, null=True)
    created = models.DateTimeField()
    created_by = models.CharField(max_length=12)
    changed = models.DateTimeField()
    changed_by = models.CharField(max_length=12)
    change_reason = models.CharField(max_length=16)
    change_comment = models.CharField(max_length=256, blank=True, null=True)
    archived = models.DateTimeField()
    archived_by = models.CharField(max_length=12)
    x_koord = models.IntegerField(blank=True, null=True)
    y_koord = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'hoone_arh'


class LastAnalysedRec(models.Model):
    process_name = models.CharField(primary_key=True, max_length=30)
    table_name = models.CharField(max_length=12)
    last_archived_id = models.IntegerField()
    date_created = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=30, blank=True, null=True)
    date_last_change = models.DateTimeField(blank=True, null=True)
    last_change_by = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'last_analysed_rec'
        unique_together = (('process_name', 'table_name'),)


class LocCompRel(models.Model):
    parent_loc_comp_id = models.IntegerField(primary_key=True)
    child_loc_comp_id = models.IntegerField()
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'loc_comp_rel'
        unique_together = (('parent_loc_comp_id', 'child_loc_comp_id'),)


class LocCompRelArh(models.Model):
    parent_loc_comp_id = models.IntegerField()
    child_loc_comp_id = models.IntegerField()
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)
    date_archived = models.DateTimeField()
    archived_by = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'loc_comp_rel_arh'


class LocCompRelB17(models.Model):
    parent_loc_comp_id = models.IntegerField()
    child_loc_comp_id = models.IntegerField()
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'loc_comp_rel_b17'


class LocCompType(models.Model):
    loc_comp_type = models.CharField(primary_key=True, max_length=30)
    lct_name = models.CharField(max_length=100)
    lct_description = models.CharField(max_length=1000, blank=True, null=True)
    level_flag = models.CharField(max_length=10)
    status = models.CharField(max_length=1)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    postal_code_flag = models.CharField(max_length=1)
    state_code_flag = models.CharField(max_length=1)
    display_level = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'loc_comp_type'


class LocCompTypeRel(models.Model):
    loc_comp_type_rel_id = models.AutoField(primary_key=True)
    parent_loc_comp_type = models.CharField(max_length=30)
    child_loc_comp_type = models.CharField(max_length=30)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'loc_comp_type_rel'


class LocCompTypeTree(models.Model):
    loc_comp_type = models.CharField(max_length=30)
    parent_loc_comp_type = models.CharField(max_length=30, blank=True, null=True)
    lct_name = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'loc_comp_type_tree'


class LocComponent(models.Model):
    loc_component_id = models.AutoField(primary_key=True)
    loc_comp_type = models.CharField(max_length=30)
    loc_comp_name = models.CharField(max_length=100)
    comp_name_type = models.CharField(max_length=50, blank=True, null=True)
    comp_name_suffix = models.CharField(max_length=20, blank=True, null=True)
    state_code = models.CharField(max_length=10, blank=True, null=True)
    postal_code = models.CharField(max_length=10, blank=True, null=True)
    add_info = models.CharField(max_length=1000, blank=True, null=True)
    active_from = models.DateTimeField()
    status = models.CharField(max_length=1)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)
    old_code = models.CharField(max_length=18, blank=True, null=True)
    parent_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'loc_component'


class LocComponentArh(models.Model):
    loc_component_id = models.IntegerField(blank=True, null=True)
    loc_comp_type = models.CharField(max_length=30)
    loc_comp_name = models.CharField(max_length=100)
    comp_name_type = models.CharField(max_length=50, blank=True, null=True)
    comp_name_suffix = models.CharField(max_length=20, blank=True, null=True)
    state_code = models.CharField(max_length=10, blank=True, null=True)
    postal_code = models.CharField(max_length=10, blank=True, null=True)
    add_info = models.CharField(max_length=1000, blank=True, null=True)
    active_from = models.DateTimeField()
    status = models.CharField(max_length=1, blank=True, null=True)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)
    old_code = models.CharField(max_length=18, blank=True, null=True)
    parent_id = models.IntegerField(blank=True, null=True)
    date_archived = models.DateTimeField()
    archived_by = models.CharField(max_length=30)
    action = models.CharField(max_length=1, blank=True, null=True)
    loc_component_arh_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'loc_component_arh'


class LocComponentB17(models.Model):
    loc_component_id = models.IntegerField()
    loc_comp_type = models.CharField(max_length=30)
    loc_comp_name = models.CharField(max_length=100)
    comp_name_type = models.CharField(max_length=50, blank=True, null=True)
    comp_name_suffix = models.CharField(max_length=20, blank=True, null=True)
    state_code = models.CharField(max_length=10, blank=True, null=True)
    postal_code = models.CharField(max_length=10, blank=True, null=True)
    add_info = models.CharField(max_length=1000, blank=True, null=True)
    active_from = models.DateTimeField()
    status = models.CharField(max_length=1)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    change_reason = models.CharField(max_length=20, blank=True, null=True)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)
    old_code = models.CharField(max_length=18, blank=True, null=True)
    parent_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'loc_component_b17'


class LocComponentTree(models.Model):
    loc_component_id = models.IntegerField()
    parent_loc_comp_id = models.IntegerField(blank=True, null=True)
    loc_comp_name = models.CharField(max_length=100)
    loc_comp_type = models.CharField(max_length=30)
    display_level = models.IntegerField()
    level_flag = models.CharField(max_length=10)

    class Meta:
        managed = False
        db_table = 'loc_component_tree'


class LocationChanges(models.Model):
    building_id_old = models.IntegerField(blank=True, null=True)
    building_id = models.IntegerField(blank=True, null=True)
    building_name = models.CharField(max_length=200, blank=True, null=True)
    building_subtype = models.CharField(max_length=20, blank=True, null=True)
    adr_id_old = models.IntegerField(blank=True, null=True)
    adr_id = models.IntegerField(blank=True, null=True)
    adr_id_type = models.CharField(max_length=20, blank=True, null=True)
    adr_string = models.CharField(max_length=1000, blank=True, null=True)
    adr_region = models.CharField(max_length=160, blank=True, null=True)
    adr_city_county = models.CharField(max_length=160, blank=True, null=True)
    adr_municipality = models.CharField(max_length=160, blank=True, null=True)
    adr_street_bg_name = models.CharField(max_length=160, blank=True, null=True)
    adr_bg_num = models.CharField(max_length=100, blank=True, null=True)
    postal_code = models.CharField(max_length=100, blank=True, null=True)
    coord_x = models.IntegerField(blank=True, null=True)
    coord_y = models.IntegerField(blank=True, null=True)
    room_id = models.CharField(max_length=160, blank=True, null=True)
    room_name = models.CharField(max_length=160, blank=True, null=True)
    floor = models.CharField(max_length=160, blank=True, null=True)
    entrance = models.CharField(max_length=160, blank=True, null=True)
    room_type = models.CharField(max_length=160, blank=True, null=True)
    add_information = models.CharField(max_length=160, blank=True, null=True)
    sys_changed_id = models.IntegerField(blank=True, null=True)
    action = models.CharField(max_length=2, blank=True, null=True)
    record_type = models.CharField(max_length=2, blank=True, null=True)
    processed = models.CharField(max_length=1, blank=True, null=True)
    processed_dtime = models.DateTimeField(blank=True, null=True)
    created_dtime = models.DateTimeField(blank=True, null=True)
    modified_dtime = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'location_changes'


class LogCfg(models.Model):
    cid = models.CharField(primary_key=True, max_length=100)
    clevel = models.IntegerField(blank=True, null=True)
    cuser = models.CharField(max_length=50, blank=True, null=True)
    cuser_source = models.CharField(max_length=10, blank=True, null=True)
    cchannel = models.IntegerField(blank=True, null=True)
    ccreated_by = models.CharField(max_length=50, blank=True, null=True)
    ccreated_date = models.DateTimeField(blank=True, null=True)
    cupdated_by = models.CharField(max_length=50, blank=True, null=True)
    cupdated_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'log_cfg'


class LogTbl(models.Model):
    caudsid = models.CharField(max_length=30, blank=True, null=True)
    cid = models.CharField(max_length=100, blank=True, null=True)
    ctime = models.DateTimeField(blank=True, null=True)
    cdb_user = models.CharField(max_length=30, blank=True, null=True)
    capp_user = models.CharField(max_length=30, blank=True, null=True)
    clevel = models.IntegerField(blank=True, null=True)
    cmsg = models.CharField(max_length=4000, blank=True, null=True)
    cmsg_clob = models.CharField(max_length=1, blank=True, null=True)
    log_tbl_id = models.IntegerField(blank=True, null=True)
    apex_info = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'log_tbl'


class Logg(models.Model):
    logg_id = models.AutoField(primary_key=True)
    text = models.CharField(max_length=2000)
    created_tdate = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'logg'


class MailLog(models.Model):
    ctime = models.DateTimeField(blank=True, null=True)
    cmailhost = models.CharField(max_length=1000, blank=True, null=True)
    cmailport = models.CharField(max_length=300, blank=True, null=True)
    csender = models.CharField(max_length=1000, blank=True, null=True)
    crcpts = models.CharField(max_length=3000, blank=True, null=True)
    csubject = models.CharField(max_length=3000, blank=True, null=True)
    cmessage = models.CharField(max_length=3000, blank=True, null=True)
    ccontent_type = models.CharField(max_length=1000, blank=True, null=True)
    cerr_msg = models.CharField(max_length=3000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mail_log'


class MeTmp(models.Model):
    kl_nr = models.IntegerField(blank=True, null=True)
    region1 = models.CharField(max_length=100, blank=True, null=True)
    city_county1 = models.CharField(max_length=100, blank=True, null=True)
    municipality1 = models.CharField(max_length=100, blank=True, null=True)
    street_bg_name1 = models.CharField(max_length=100, blank=True, null=True)
    bg_num1 = models.CharField(max_length=100, blank=True, null=True)
    adr_reg_id1 = models.IntegerField(blank=True, null=True)
    buidling_id1 = models.IntegerField(blank=True, null=True)
    t_eesnimi1 = models.CharField(max_length=100, blank=True, null=True)
    t_perenimi1 = models.CharField(max_length=100, blank=True, null=True)
    t_id1 = models.IntegerField(blank=True, null=True)
    t_eesnimi2 = models.CharField(max_length=100, blank=True, null=True)
    t_perenimi2 = models.CharField(max_length=100, blank=True, null=True)
    t_id2 = models.IntegerField(blank=True, null=True)
    tj_id = models.IntegerField(blank=True, null=True)
    ith_kl_id = models.IntegerField(blank=True, null=True)
    coord_x = models.IntegerField(blank=True, null=True)
    coord_y = models.IntegerField(blank=True, null=True)
    adr_reg_id = models.IntegerField(blank=True, null=True)
    adr_region = models.CharField(max_length=100, blank=True, null=True)
    adr_city_county = models.CharField(max_length=100, blank=True, null=True)
    adr_municipality = models.CharField(max_length=100, blank=True, null=True)
    adr_street_bg_name = models.CharField(max_length=100, blank=True, null=True)
    adr_bg_num = models.CharField(max_length=100, blank=True, null=True)
    adr_string2 = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'me_tmp'


class Msg(models.Model):
    msg_id = models.AutoField(primary_key=True)
    lang1 = models.CharField(max_length=4000, blank=True, null=True)
    lang2 = models.CharField(max_length=4000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'msg'


class OldAddrRel(models.Model):
    address_id = models.IntegerField(primary_key=True)
    asum_id = models.CharField(max_length=10)
    tan_id = models.CharField(max_length=10, blank=True, null=True)
    hg_kood = models.CharField(max_length=10)
    adr_id = models.CharField(max_length=10, blank=True, null=True)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'old_addr_rel'
        unique_together = (('address_id', 'hg_kood'),)


class RegionListV(models.Model):
    id = models.IntegerField(primary_key=True)
    region = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'region_list_v'


class Room(models.Model):
    room_id = models.AutoField(primary_key=True)
    room_name = models.CharField(max_length=20)
    room_type = models.CharField(max_length=50)
    b_object_id = models.IntegerField()
    add_information = models.CharField(max_length=1000, blank=True, null=True)
    created = models.DateTimeField()
    created_by = models.CharField(max_length=12)
    changed = models.DateTimeField()
    changed_by = models.CharField(max_length=12)
    change_reason = models.CharField(max_length=16)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)
    valid_from = models.DateTimeField(blank=True, null=True)
    valid_until = models.DateTimeField(blank=True, null=True)
    floor = models.CharField(max_length=32, blank=True, null=True)
    entrance = models.CharField(max_length=32, blank=True, null=True)
    address_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'room'


class RoomArh(models.Model):
    room_id = models.IntegerField(blank=True, null=True)
    room_name = models.CharField(max_length=20, blank=True, null=True)
    room_type = models.CharField(max_length=50, blank=True, null=True)
    b_object_id = models.IntegerField(blank=True, null=True)
    add_information = models.CharField(max_length=1000, blank=True, null=True)
    created = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=12, blank=True, null=True)
    changed = models.DateTimeField(blank=True, null=True)
    changed_by = models.CharField(max_length=12, blank=True, null=True)
    change_reason = models.CharField(max_length=16, blank=True, null=True)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)
    valid_from = models.DateTimeField(blank=True, null=True)
    valid_until = models.DateTimeField(blank=True, null=True)
    floor = models.CharField(max_length=10, blank=True, null=True)
    entrance = models.CharField(max_length=10, blank=True, null=True)
    date_archived = models.DateTimeField(blank=True, null=True)
    archived_by = models.CharField(max_length=30, blank=True, null=True)
    room_arh_id = models.IntegerField(blank=True, null=True)
    action = models.CharField(max_length=1, blank=True, null=True)
    address_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'room_arh'


class RoomChanged(models.Model):
    room_changed_id = models.AutoField(primary_key=True)
    room_id = models.IntegerField(blank=True, null=True)
    building_id = models.IntegerField(blank=True, null=True)
    processed = models.CharField(max_length=1, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=30, blank=True, null=True)
    date_changed = models.DateTimeField(blank=True, null=True)
    changed_by = models.CharField(max_length=30, blank=True, null=True)
    date_processed = models.DateTimeField(blank=True, null=True)
    processed_by = models.CharField(max_length=30, blank=True, null=True)
    address_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'room_changed'


class Ruum(models.Model):
    ruum = models.CharField(max_length=20)
    ruumityyp = models.CharField(max_length=50)
    hoone_id = models.IntegerField()
    lisainfo = models.CharField(max_length=1000, blank=True, null=True)
    created = models.DateTimeField()
    created_by = models.CharField(max_length=12)
    changed = models.DateTimeField()
    changed_by = models.CharField(max_length=12)
    change_reason = models.CharField(max_length=16)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)
    valid_from = models.DateTimeField(blank=True, null=True)
    valid_until = models.DateTimeField(blank=True, null=True)
    korrus = models.CharField(max_length=10, blank=True, null=True)
    trepikoda = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ruum'


class RuumArh(models.Model):
    archive_id = models.AutoField(primary_key=True)
    id = models.IntegerField()
    ruum = models.CharField(max_length=20)
    ruumityyp = models.CharField(max_length=50)
    hoone_id = models.IntegerField()
    lisainfo = models.CharField(max_length=1000, blank=True, null=True)
    created = models.DateTimeField()
    created_by = models.CharField(max_length=12)
    changed = models.DateTimeField()
    changed_by = models.CharField(max_length=12)
    change_reason = models.CharField(max_length=16)
    change_comment = models.CharField(max_length=1000, blank=True, null=True)
    valid_from = models.DateTimeField(blank=True, null=True)
    valid_until = models.DateTimeField(blank=True, null=True)
    archived = models.DateTimeField()
    archived_by = models.CharField(max_length=12)
    korrus = models.CharField(max_length=10, blank=True, null=True)
    trepikoda = models.CharField(max_length=10, blank=True, null=True)
    aadress_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ruum_arh'


class SplitMergeRule(models.Model):
    split_merge_rule_id = models.AutoField(primary_key=True)
    old_loc_comp_type = models.CharField(max_length=30)
    new_loc_comp_type = models.CharField(max_length=30)
    date_created = models.DateTimeField()
    created_by = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'split_merge_rule'


class Sysglobal(models.Model):
    name = models.CharField(max_length=30, blank=True, null=True)
    value = models.CharField(max_length=4000, blank=True, null=True)
    type = models.CharField(max_length=20, blank=True, null=True)
    table_name = models.CharField(max_length=30, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=100, blank=True, null=True)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=100, blank=True, null=True)
    format = models.CharField(max_length=20, blank=True, null=True)
    org_code = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sysglobal'


class SystemAddrChanged(models.Model):
    system_addr_changed_id = models.AutoField(primary_key=True)
    addr_changed_id = models.IntegerField(blank=True, null=True)
    address_id = models.IntegerField(blank=True, null=True)
    id_type = models.CharField(max_length=30, blank=True, null=True)
    elion_system_id = models.IntegerField(blank=True, null=True)
    addr_changed_status = models.CharField(max_length=1, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=30, blank=True, null=True)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    new = models.CharField(max_length=1, blank=True, null=True)
    action = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'system_addr_changed'


class SystemAddrChangedLog(models.Model):
    system_addr_changed_log_id = models.AutoField(primary_key=True)
    address_id = models.IntegerField(blank=True, null=True)
    id_type = models.CharField(max_length=30, blank=True, null=True)
    elion_system_id = models.IntegerField(blank=True, null=True)
    result_code = models.CharField(max_length=10, blank=True, null=True)
    error_text = models.CharField(max_length=1000, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=30, blank=True, null=True)
    sent = models.CharField(max_length=1, blank=True, null=True)
    new = models.CharField(max_length=1, blank=True, null=True)
    sys_addr_changed_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'system_addr_changed_log'


class SystemBuildingChanged(models.Model):
    system_building_changed_id = models.AutoField(primary_key=True)
    system_addr_changed_id = models.IntegerField(blank=True, null=True)
    action = models.CharField(max_length=1, blank=True, null=True)
    building_id = models.IntegerField(blank=True, null=True)
    elion_system_id = models.IntegerField(blank=True, null=True)
    building_changed_status = models.CharField(max_length=1, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=30, blank=True, null=True)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)
    b_object_changed_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'system_building_changed'


class SystemRoomChanged(models.Model):
    system_room_changed_id = models.AutoField(primary_key=True)
    system_building_changed_id = models.IntegerField(blank=True, null=True)
    action = models.CharField(max_length=1, blank=True, null=True)
    room_id = models.IntegerField(blank=True, null=True)
    room_changed_id = models.IntegerField(blank=True, null=True)
    elion_system_id = models.IntegerField(blank=True, null=True)
    room_changed_status = models.CharField(max_length=1, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=30, blank=True, null=True)
    date_last_changed = models.DateTimeField(blank=True, null=True)
    last_changed_by = models.CharField(max_length=30, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'system_room_changed'


class TmpRegionChanges(models.Model):
    seq = models.IntegerField(blank=True, null=True)
    id = models.IntegerField(primary_key=True)
    new_name = models.CharField(max_length=100, blank=True, null=True)
    old_name = models.CharField(max_length=100, blank=True, null=True)
    sent = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_region_changes'

from django.contrib import admin
from django.apps import apps
from django.contrib.admin.sites import AlreadyRegistered

from ress_api_admin.locations.models import Address


class MultiDBModelAdmin(admin.ModelAdmin):
    using = 'locations'

    def save_model(self, request, obj, form, change):
        obj.save(using=self.using)

    def delete_model(self, request, obj):
        obj.delete(using=self.using)

    def get_queryset(self, request):
        return super().get_queryset(request).using(self.using)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        return super().formfield_for_foreignkey(db_field, request, using=self.using, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        return super().formfield_for_manytomany(db_field, request, using=self.using, **kwargs)


models = apps.get_models()

for model in models:
    try:
        if 'ress_api_admin.locations' in model.__module__:
            options = {}
            model_fields = []
            for field in model._meta.get_fields():
                if field.__module__ == 'django.db.models.fields':
                    if field.name is not 'id':
                        model_fields.append(field.name)
            # options['fields'] = model_fields
            if model == Address:
                # self.bg_num, self.street_bg_name, self.municipality, self.city_county, self.region
                options['list_display'] = ('address_id',
                                           # '__str__',
                                           'bg_num',
                                           'street_bg_name',
                                           'municipality',
                                           'city_county',
                                           'region',
                                           'postal_code',
                                           'hg_kood',
                                           'date_last_changed',
                                           'date_created')
                options['search_fields'] = ('add_information', 'addr_string1', 'addr_string2',
                                            'address_id', 'address_type', 'bg_num', 'bg_num_id',
                                            'change_comment', 'change_reason', 'city_county',
                                            'city_county_id', 'created_by', 'date_created',
                                            'date_last_changed', 'hg_kood', 'last_changed_by',
                                            'municipality', 'municipality_id', 'postal_code',
                                            'region', 'region_id', 'rel_addr_flag', 'status',
                                            'street_bg_name', 'street_bg_name_id')
                options['list_filter'] = ('date_created', 'date_last_changed', 'region')
            else:
                options = {}
            admin.site.register(model, MultiDBModelAdmin, **options)
    except AlreadyRegistered:
        pass

import os
from datetime import datetime
import sys
import re
import urllib.request
from urllib.parse import urljoin
import shutil
import tarfile

from bs4 import BeautifulSoup
from invoke import task
from invoke.main import program

APP_NAME = 'address-search'


@task
def clean(c):
    shutil.rmtree('build')

@task
def build_address_popup(c, 
                        debug=False, 
                        search_root_url='http://localhost:8000/address-search/'):
    """ Build popup static SPA when running Django locally with production settings:
    docker-compose -f production.yml up. This package should be deployed to:
        DEV: anette@conga.cc.elion.ee:/home/anette/httpd_anette/address-search
        TEST: ...
        LIVE: ...
    """
    build_dir = os.path.join('build', APP_NAME)
    print(f'Cleaning up previous address-search build directory {build_dir}')
    if os.path.exists(build_dir):
        shutil.rmtree(build_dir)
    os.makedirs(build_dir)

    print(f'Downloading index page from {search_root_url}')
    req = urllib.request.urlopen(search_root_url)
    response = req.read()

    print('Downloading SPA assets')
    soup = BeautifulSoup(response, features='html.parser')
    for asset in soup.find_all(['link', 'script']):
        for ref_attr in ['href', 'src']:
            if ref_attr in asset.attrs:
                url = asset[ref_attr]
                if url.find('/static/') == 0:
                    asset[ref_attr] = f'.{url}'  # Lets make all URL-s relative for deployment
                    asset_url = urljoin(search_root_url, url)
                    req = urllib.request.urlopen(asset_url)
                    response = req.read()

                    asset_path = os.path.join(build_dir, os.path.normpath(url[1:]))
                    dirname = os.path.dirname(asset_path)
                    os.makedirs(dirname, exist_ok=True)

                    with open(asset_path, mode='wb') as file_handle:
                        file_handle.write(response)

    # Make all links relative for easier deployment setups
    for asset in soup.find_all('a'):
        url = asset['href']
        asset['href'] = url.replace(f'/{APP_NAME}', '.')

    index_file = os.path.join(build_dir, 'index.html')
    print(f'Writing index page to disk {index_file}')
    with open(index_file, mode='w', encoding='utf-8') as file_handle:
        file_handle.write(soup.prettify())

    timestamp = datetime.strftime(datetime.now(), '%Y-%m-%d-%H-%M')
    deployment_file = os.path.join('build', f'{APP_NAME}_{timestamp}.tar.gz')
    print(f'Creating archive {deployment_file}')
    with tarfile.open(deployment_file, mode='w:gz') as archive:
        archive.add(build_dir, arcname=os.path.basename(build_dir), recursive=True)
    print(f'Archive created')

    if not debug:
        print(f'Cleaning up build folder {build_dir}')
        shutil.rmtree(build_dir)


if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    sys.exit(program.run())
